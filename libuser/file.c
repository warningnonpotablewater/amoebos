#include <user/file.h>

bool _file_open(struct file_handle *, str);

MaybeFileHandle file_open(str name) {
    struct file_handle handle = {0};
    MaybeFileHandle result = Nothing;

    bool success = _file_open(&handle, name);

    result.value = handle;
    result.present = success;

    return result;
}
