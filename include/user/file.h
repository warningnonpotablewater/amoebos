#ifndef USER_FILE_H
#define USER_FILE_H

#include <runtime/primitives.h>

// Abstract representation of a file.
struct file_handle {
    const u8 *data; // Points to the contents of the file.
    ulen size; // How many bytes long the file is.
};

typedef Maybe(struct file_handle) MaybeFileHandle;

// Attempts to open a file with the given name.
//
// # Arguments
//
// * `name` - Name of the file to open.
//
// # Returns
//
// Optional file handle if the file exists.
MaybeFileHandle file_open(str);

// Attempts to execute a given binary file.
//
// # Arguments
//
// * `name` - Name of the file to execute.
//
// # Returns
//
// If the file doesn't exist.
void file_execute(str);

#endif
