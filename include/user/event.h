#ifndef USER_EVENT_H
#define USER_EVENT_H

#include <runtime/primitives.h>

#include <platform/abi.h>

// Handles the event in a user-intended way. Must call `event_return` before
// terminating.
//
// # Arguments
//
// * `type` - Type of the event.
typedef void EventCallback(ulen);

// Sets the event handler for the current program.
//
// # Arguments
//
// * `callback` - Function to use as the handler.
void event_set_handler(EventCallback *);

// Removes the event handler for the current program. Does nothing if one hasn't
// been set before.
void event_remove_handler(void);

// Finishes handling the event and returns the program to where it left off. If
// called outside of an event callback, the behavior is undefined.
void event_return(void);

#endif
