#ifndef USER_SYSTEM_H
#define USER_SYSTEM_H

#include <runtime/primitives.h>

// Prints a user-supplied message on the display.
//
// # Arguments
//
// * `x` - Horizontal position in pixels.
// * `y` - Vertical position in pixels.
// * `color` - Color of the glyphs.
// * `message` - Message to be printed.
void system_debug(s16, s16, u8, str);

#endif
