#ifndef RUNTIME_ARRAY_H
#define RUNTIME_ARRAY_H

#include <runtime/primitives.h>

// Compares 2 array elements in a user-intended way.
//
// # Arguments
//
// * `a`, `b` - 2 elements to compare.
//
// # Returns
//
// * Negative value if `a` < `b`.
// * Zero if `a` == `b`.
// * Positive value if `a` > `b`.
typedef slen ArrayCompareFunction(const void *, const void *);

// Compares 2 arrays until it encounters an element that's different.
//
// # Arguments
//
// * `a`, `b` - 2 arrays to compare.
// * `element_size`- Byte size of individual array elements.
// * `length` - Length of the arrays in `element_size` chunks.
// * `callback` - User-supplied function that compares 2 adjacent elements.
//
// # Returns
//
// * Negative value if `a` < `b`.
// * Zero if `a` == `b`.
// * Positive value if `a` > `b`.
slen array_compare(
    const void *, const void *, ulen, ulen, ArrayCompareFunction *
);

// Attempts to find the first/last element of an array, equal to the one being
// searched for.
//
// # Arguments
//
// * `haystack` - Array to search through.
// * `needle` - Element to find.
// * `element_size`- Byte size of individual array elements.
// * `length` - Length of the array in `element_size` chunks.
// * `callback` - User-supplied function that compares the current element with
//   the one being searched for.
//
// # Returns
//
// Optional index of the element inside the array.
MaybeUlen array_find(
    const void *, const void *, ulen, ulen, ArrayCompareFunction *
);
MaybeUlen array_find_last(
    const void *, const void *, ulen, ulen, ArrayCompareFunction *
);

// Attempts to find an element inside the sorted array using binary search.
//
// # Arguments
//
// * `haystack` - Array to search through.
// * `needle` - Element to find.
// * `element_size`- Byte size of individual array elements.
// * `length` - Length of the array in `element_size` chunks.
// * `callback` - User-supplied function that compares the current element with
//   the one being searched for.
//
// # Returns
//
// Optional index of the element inside the array.
MaybeUlen array_binary_search(
    const void *, const void *, ulen, ulen, ArrayCompareFunction *
);

// Reverses the elements of an array.
//
// # Arguments
//
// * `array` - Array to reverse.
// * `element_size`- Byte size of individual array elements.
// * `length` - Length of the array in `element_size` chunks.
void array_reverse(void *, ulen, ulen);

// Moves the items in an array to the right by the specified amount, wrapping
// around to the other side.
//
// # Arguments
//
// * `array` - Array to reverse.
// * `element_size`- Byte size of individual array elements.
// * `length` - Length of the array in `element_size` chunks.
// * `amount` - How many elements to move. Capped at the length of the array.
void array_rotate(void *, ulen, ulen, ulen);

// Checks whether the array is sorted in an ascending order.
//
// # Arguments
//
// * `array` - Array to check.
// * `element_size`- Byte size of individual array elements.
// * `length` - Length of the array in `element_size` chunks.
// * `callback` - User-supplied function that compares 2 adjacent elements.
//
// # Returns
//
// Whether the array is sorted.
bool array_is_sorted(const void *, ulen, ulen, ArrayCompareFunction *);

// Finds the maximum element in an array.
//
// # Arguments
//
// * `array` - Array to check.
// * `element_size`- Byte size of individual array elements.
// * `length` - Length of the array in `element_size` chunks.
// * `callback` - User-supplied function that compares the current maximum with
//   the current element.
//
// # Returns
//
// Position of the maximum element or 0 if there isn't one.
ulen array_maximum(const void *, ulen, ulen, ArrayCompareFunction *);

// Sorts the array using an unstable/stable sorting algorithm.
//
// # Arguments
//
// * `array` - Array to sort.
// * `element_size`- Byte size of individual array elements.
// * `length` - Length of the array in `element_size` chunks.
// * `callback` - User-supplied function that compares 2 adjacent elements.
void array_unstable_sort(void *, ulen, ulen, ArrayCompareFunction *);
void array_stable_sort(void *, ulen, ulen, ArrayCompareFunction *);

// Discards the specified range of elements, moves the rest into their place,
// and zero-fills the rest of the array.
//
// # Arguments
//
// * `array` - Array to manipulate.
// * `element_size`- Byte size of individual array elements.
// * `length` - Length of the array in `element_size` chunks.
// * `index` - Index of the first element to displace. Nothing is done if it's
//   out of bounds.
// * `amount` - The number of elements to displace. Capped at the length of the
//   array. Nothing is done if it's zero.
//
// # Returns
//
// Number of elements displaced.
ulen array_displace(void *, ulen, ulen, ulen, ulen);

// Moves the specified range of elements to the start of the array and
// zero-fills everything else.
//
// # Arguments
//
// * `array` - Array to manipulate.
// * `element_size`- Byte size of individual array elements.
// * `length` - Length of the array in `element_size` chunks.
// * `index` - Index of the first element to isolate. Nothing is done if it's
//   out of bounds.
// * `amount` - The number of elements to isolate. Capped at the length of the
//   array. Nothing is done if it's zero.
//
// # Returns
//
// Number of elements isolated.
ulen array_isolate(void *, ulen, ulen, ulen, ulen);

#endif
