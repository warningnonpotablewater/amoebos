#ifndef RUNTIME_STRING_H
#define RUNTIME_STRING_H

#include <runtime/primitives.h>

// Measures the length of a string.
//
// # Arguments
//
// * `string` - String to measure.
//
// # Returns
//
// Byte length of the string, without counting the terminating null-byte.
ulen string_length(str);

// Compares 2 strings until it encounters a byte that's different.
//
// # Arguments
//
// * `a`, `b` - 2 strings to compare.
//
// # Returns
//
// * Negative value if `a` < `b`.
// * Zero if `a` == `b`.
// * Positive value if `a` > `b`.
slen string_compare(str, str);

// Attempts to find a byte inside the string.
//
// # Arguments
//
// * `string` - String to search through.
// * `byte` - Byte to find.
//
// # Returns
//
// Optional position of the byte inside the string.
MaybeUlen string_find_byte(str, u8);

// Attempts to find a substring inside the string.
//
// # Arguments
//
// * `haystack` - String to search through.
// * `needle` - Substring to find.
//
// # Returns
//
// Optional position of the first byte of the substring inside the string.
MaybeUlen string_find_string(str, str);

// Attempts to parse the string as an unsigned/signed integer.
//
// # Arguments
//
// * `string` - String to parse.
// * `base` - Base of the number. Must be between 2 and 36 or nothing is
//   returned.
//
// # Returns
//
// Optional parsed number.
MaybeUlen string_parse_ulen(str, u8);
MaybeSlen string_parse_slen(str, u8);

#endif
