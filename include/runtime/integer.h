#ifndef RUNTIME_INTEGER_H
#define RUNTIME_INTEGER_H

#include <runtime/primitives.h>

// Converts an unsigned number to a signed number by adding a sign to it.
//
// # Arguments
//
// * `negative` - Whether the number should be negative.
// * `number` - Number to add a sign to.
//
// # Returns
//
// Optional number with the added sign if no overflow occured in the process.
MaybeSlen ulen_add_sign(bool, ulen);

// Converts a signed number to an unsigned number by removing its sign.
// Guaranteed to produce the correct value for `SLEN_MIN`.
//
// # Arguments
//
// * `number` - Number to remove the sign from.
//
// # Returns
//
// Unsigned absolute value of the provided number.
ulen slen_remove_sign(slen);

// Attempts to safely perform a mathematical operation on 2 unsigned numbers.
//
// # Arguments
//
// * `a`, `b` - Numbers to perform the operation on.
//
// # Returns
//
// Optional result of the operation if it won't cause an overflow or undefined
// behavior in the process.
MaybeUlen ulen_add(ulen, ulen);
MaybeUlen ulen_subtract(ulen, ulen);
MaybeUlen ulen_multiply(ulen, ulen);
MaybeUlen ulen_divide(ulen, ulen);
MaybeUlen ulen_remainder(ulen, ulen);
MaybeUlen ulen_shift_left(ulen, ulen);
MaybeUlen ulen_shift_right(ulen, ulen);

// Attempts to safely perform a mathematical operation on 2 signed numbers.
//
// # Arguments
//
// * `a`, `b` - Numbers to perform the operation on.
//
// # Returns
//
// Optional result of the operation if it won't cause undefined behavior in the
// process.
MaybeSlen slen_add(slen, slen);
MaybeSlen slen_subtract(slen, slen);
MaybeSlen slen_multiply(slen, slen);
MaybeSlen slen_divide(slen, slen);
MaybeSlen slen_remainder(slen, slen);
MaybeSlen slen_shift_left(slen, slen);
MaybeSlen slen_shift_right(slen, slen);

// Slices the specified range of bits off an unsigned number.
//
// # Arguments
//
// * `number` - Number to slice the bits off.
// * `start` - Position of the lowest bit to slice.
// * `end` - Position of the highest bit to slice.
//
// # Returns
//
// The bits in the closed [`start`; `end`] range. If the number of bits is
// negative, 0 is returned. If the number of bits is greater than `XLEN_BITS`,
// the original number is returned.
ulen ulen_slice_bits(ulen, u8, u8);

// Clears the specified range of bits in an unsigned number.
//
// # Arguments
//
// * `number` - Number to clear the bits in.
// * `start` - Position of the lowest bit to clear.
// * `end` - Position of the highest bit to clear.
//
// # Returns
//
// Number with the bits in the closed [`start`; `end`] range cleared. If the
// number of bits is negative, the original number is returned. If the number of
// bits is greater than `XLEN_BITS`, 0 is returned.
ulen ulen_clear_bits(ulen, u8, u8);

#endif
