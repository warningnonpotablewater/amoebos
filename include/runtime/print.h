#ifndef RUNTIME_PRINT_H
#define RUNTIME_PRINT_H

#include <runtime/primitives.h>

// Calls `print` while automatically calculating the maximum size of the data.
#define PRINT(data, recipe, ...)\
    (print(data, sizeof(data), STRING_LITERAL(recipe), __VA_ARGS__))

// Formats a string according to the recipe and puts it in the array. The array
// may contain an existing string, in which case the formatted data will be
// appended. Guarantees to always null-terminate the string.
//
// # Arguments
//
// * `data` - Array to put the formatted string into.
// * `size` - Size of the array.
// * `recipe` - POSIX-compatible subset of the `sprintf` format string with the
//   following conversion characters supported: `idouxcsp%`. Note that the `l`
//   flag is implied for integer conversions and other optional modifiers aren't
//   supported.
//
// # Returns
//
// The number of characters printed.
ulen print(u8 *, ulen, str, ...);

#endif
