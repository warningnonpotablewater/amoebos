#ifndef RUNTIME_FORMAT_H
#define RUNTIME_FORMAT_H

#include <runtime/primitives.h>

// Implementation detail. Don't rely on it.
struct format_buffer {
    u8 *data;
    ulen capacity;
    ulen used;
};

// Calls `format_buffer_new` while automatically calculating the maximum size of
// the data.
#define FORMAT_BUFFER(data) (format_buffer_new((data), sizeof(data)))

// Wraps a character array, allowing to use it for string formatting. The array
// may contain an existing string, in which case the formatted data will be
// appended. Guarantees to always null-terminate the strings.
//
// # Arguments
//
// * `data` - Array to put the formatted string into.
// * `size` - Size of the array.
//
// # Returns
//
// Format buffer that must be treated as an opaque struct.
struct format_buffer format_buffer_new(u8 *, ulen);

// Shrinks the size of the work area of the format buffer to the space
// available.
//
// # Arguments
//
// * `buffer` - Format buffer.
void format_continue(struct format_buffer *);

// Fills the format buffer with zeroes and resets its state.
//
// # Arguments
//
// * `buffer` - Format buffer.
void format_clear(struct format_buffer *);

// Appends a character to the format buffer.
//
// # Arguments
//
// * `buffer` - Format buffer.
// * `character` - Character to append.
//
// # Returns
//
// Whether the character fit in the buffer.
bool format_character(struct format_buffer *, u8);

// Appends a memory region to the format buffer.
//
// # Arguments
//
// * `buffer` - Format buffer.
// * `source` - Region to append.
// * `size` - How many bytes to append.
//
// # Returns
//
// Whether the string fit in the buffer completely.
bool format_memory(struct format_buffer *, const void *, ulen);

// Appends a string to the format buffer.
//
// # Arguments
//
// * `buffer` - Format buffer.
// * `string` - String to append.
//
// # Returns
//
// Whether the string fit in the buffer completely.
bool format_string(struct format_buffer *, str);

// Appends an unsigned/signed integer to the format buffer.
//
// # Arguments
//
// * `buffer` - Format buffer.
// * `number` - Number to append.
// * `base` - Base of the number. Must be between 2 and 36 or the number is
//   treated as 0.
// * `padding` - How many digits to pad the number to with zeroes. Capped at
//   `XLEN_BITS`.
//
// # Returns
//
// Whether the number fit in the buffer completely.
bool format_ulen(struct format_buffer *, ulen, u8, u8);
bool format_slen(struct format_buffer *, slen, u8, u8);

// Pads the buffer to the specified number of characters from the start/the end.
// The padding itself gets truncated if it doesn't fit in the buffer.
//
// # Arguments
//
// * `buffer` - Format buffer.
// * `padding` - How many characters to pad to.
// * `character` - Which character to pad with.
//
// # Returns
//
// Whether the padding fit in the buffer completely.
bool format_pad_start(struct format_buffer *, ulen, u8);
bool format_pad_end(struct format_buffer *, ulen, u8);

// Discards the specified range of characters and moves the rest into their
// place.
//
// # Arguments
//
// * `buffer` - Format buffer.
// * `index` - Index of the first character to displace. Nothing is done if it's
//   out of bounds.
// * `amount` - The number of characters to displace. Capped at the length of
//   the buffer. Nothing is done if it's zero.
void format_displace(struct format_buffer *, ulen, ulen);

// Moves the specified range of characters to the start of the buffer and
// discards the rest.
//
// # Arguments
//
// * `buffer` - Format buffer.
// * `index` - Index of the first character to isolate. Nothing is done if it's
//   out of bounds.
// * `amount` - The number of characters to isolate. Capped at the length of the
//   buffer. Nothing is done if it's zero.
void format_isolate(struct format_buffer *, ulen, ulen);

// Transforms the characters in the buffer to lowercase/uppercase.
//
// # Arguments
//
// * `buffer` - Format buffer.
void format_lowercase(struct format_buffer *);
void format_uppercase(struct format_buffer *);

#endif
