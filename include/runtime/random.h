#ifndef RUNTIME_RANDOM_H
#define RUNTIME_RANDOM_H

#include <runtime/primitives.h>

// Implementation detail. Don't rely on it.
struct random_state {
    u32 last;
};

// Initializes the state needed for random number generation.
//
// # Arguments
//
// * `seed` - Initial value to feed to the RNG algorithm.
//
// # Returns
//
// State of the RNG algorithm that must be treated as an opaque struct.
struct random_state random_new(u32);

// Obtains a raw value from the RNG algorithm.
//
// # Arguments
//
// * `state` - State of the RNG algorithm.
//
// # Returns
//
// Unsigned 32 bit integer not limited to any particular range.
u32 random_next(struct random_state *);

// Generates a random boolean value.
//
// # Arguments
//
// * `state` - State of the RNG algorithm.
//
// # Returns
//
// Either `true` or `false`.
bool random_bool(struct random_state *);

// Generates a random unsigned/signed integer.
//
// # Arguments
//
// * `state` - State of the RNG algorithm.
// * `floor` - Lower boundary of the values.
// * `ceiling` - Higher boundary of the values.
//
// # Returns
//
// 32 bit integer in the closed [`floor`; `ceiling`] range. If `floor` >
// `ceiling`, their places are swapped.
u32 random_u32(struct random_state *, u32, u32);
s32 random_s32(struct random_state *, s32, s32);

// Shuffles the array in a random order.
//
// # Arguments
//
// * `state` - State of the RNG algorithm.
// * `array` - Array to be shuffled.
// * `element_size - Byte size of individual array elements.
// * `length` - Length of the array in `element_size` chunks.
void random_shuffle(struct random_state *, void *, ulen, ulen);

#endif
