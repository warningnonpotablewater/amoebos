#ifndef RUNTIME_ASCII_H
#define RUNTIME_ASCII_H

#include <runtime/primitives.h>

// Checks if the character is non-ASCII.
//
// # Arguments
//
// * `character` - Character to check.
//
// # Returns
//
// Whether the character is outside of the 7 bit ASCII table.
bool ascii_is_non_ascii(u8);

// Checks if the character is a control/whitespace character.
//
// # Arguments
//
// * `character` - Character to check.
//
// # Returns
//
// Whether the character belongs to the `Cc` category/has `White_Space=yes`
// according to Unicode. Note that some characters can do both.
bool ascii_is_control(u8);
bool ascii_is_whitespace(u8);

// Checks if the character is a lowercase/uppercase letter.
//
// # Arguments
//
// * `character` - Character to check.
//
// # Returns
//
// Whether the character is lowercase/uppercase. If the character is not a
// letter, `false` is returned.
bool ascii_is_lowercase(u8);
bool ascii_is_uppercase(u8);

// Checks if the character is a decimal digit.
//
// # Arguments
//
// * `character` - Character to check.
//
// # Returns
//
// Whether the character is a decimal digit. If the character is not a digit,
// `false` is returned.
bool ascii_is_decimal(u8);

// Checks if the character is a math/currency symbol or a punctuation mark.
//
// # Arguments
//
// * `character` - Character to check.
//
// # Returns
//
// Whether the character belongs to the `P` or `S` categories according to
// Unicode.
bool ascii_is_symbol(u8);

// Transforms the characters in an array to lowercase/uppercase.
//
// # Arguments
//
// * `array` - Array to transform.
// * `size` - How many characters to transform.
void ascii_to_lowercase(u8 *, ulen);
void ascii_to_uppercase(u8 *, ulen);

// Attempts to parse the character array as an unsigned/signed integer.
//
// # Arguments
//
// * `array` - Array to parse.
// * `size` - How many characters to parse.
// * `base` - Base of the number. Must be between 2 and 36 or nothing is
//   returned.
//
// # Returns
//
// Optional parsed number.
MaybeUlen ascii_parse_ulen(const u8 *, ulen, u8);
MaybeSlen ascii_parse_slen(const u8 *, ulen, u8);

#endif
