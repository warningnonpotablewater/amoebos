#ifndef RUNTIME_MEMORY_H
#define RUNTIME_MEMORY_H

#include <runtime/primitives.h>

// Copies the contents of one memory region into the other. Guaranteed to work
// correctly when the regions overlap each other.
//
// # Arguments
//
// * `source` - Where to copy from.
// * `destination` - Where to copy to.
// * `size` - How many bytes to copy.
void memory_copy(const void *, void *, ulen);

// Compares 2 memory regions until it encounters a byte that's different.
//
// # Arguments
//
// * `a`, `b` - 2 regions to compare.
//
// # Returns
//
// * Negative value if `a` < `b`.
// * Zero if `a` == `b`.
// * Positive value if `a` > `b`.
slen memory_compare(const void *, const void *, ulen);

// Fills every byte of the memory region with the specified value.
//
// Arguments
//
// * `destination` - Region to fill.
// * `size` - How many bytes to fill.
// * `value` - Which byte to fill with.
void memory_fill(void *, ulen, u8);

// Swaps the contents of the 2 non-overlapping memory regions.
//
// # Arguments
//
// * `a`, `b` - 2 regions to swap.
// * `size` - How many bytes to swap.
void memory_swap(void *, void *, ulen);

// Attempts to find a byte inside the memory region.
//
// # Arguments
//
// * `source` - Region to search through.
// * `size` - How many bytes to search through.
// * `byte` - Byte to find.
//
// # Returns
//
// Optional index of the byte inside the region.
MaybeUlen memory_find_byte(const void *, ulen, u8);

// Attempts to find a byte sequence inside the memory region.
//
// # Arguments
//
// * `haystack` - Region to search through.
// * `haystack_size` - How many bytes to search through.
// * `needle` - Sequence to find.
// * `needle_size` - How many bytes long the sequence is.
//
// # Returns
//
// Optional index of the first byte of the sequence inside the region.
MaybeUlen memory_find_memory(const void *, ulen, const void *, ulen);

#endif
