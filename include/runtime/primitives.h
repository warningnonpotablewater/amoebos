#ifndef RUNTIME_PRIMITIVES_H
#define RUNTIME_PRIMITIVES_H

// Standard keyword macros.

#define noreturn _Noreturn
#define thread_local _Thread_local

#define alignof(type) _Alignof(type)
#define alignas(type) _Alignas(type)

#define bool _Bool
#define false ((bool) 0)
#define true ((bool) 1)

#define static_assert(expression, message) _Static_assert(expression, message)

// Standard macros that require vendor extensions.

typedef __builtin_va_list va_list;

#define va_start(ap, argn) __builtin_va_start(ap, argn)
#define va_copy(dest, src) __builtin_va_copy(dest, src)
#define va_arg(ap, type) __builtin_va_arg(ap, type)
#define va_end(ap) __builtin_va_end(ap)

#define offsetof(type, member) __builtin_offsetof(type, member)

// Non-standard but often useful macros.

#define XSTRINGIFY(token) STRINGIFY(token)
#define STRINGIFY(token) #token

#define STRING_LITERAL(literal) "" literal ""

// Unsigned and signed integer types with exact widths.

static_assert((unsigned char) 0x100 == 0, "bytes are 8 bits wide");
static_assert((-1 & 3) == 3, "signed integers are two's complement");
static_assert((signed char) 0xff == -1, "unsigned-signed casts are bit casts");
static_assert(-2 >> 1 == -1, "signed right-shifts are arithmetic shifts");

static_assert(sizeof(short) == 2, "short is 2 bytes wide");
static_assert(sizeof(int) == 4, "int is 4 bytes wide");
static_assert(sizeof(long long) == 8, "long long is 8 bytes wide");

static_assert(sizeof(void *) >= 4, "pointers are at least 4 bytes wide");
static_assert(sizeof(long) >= sizeof(void *), "long can hold a pointer");

typedef unsigned char u8;
typedef signed char s8;

static const u8 U8_MAX = 0xff;
static const u8 U8_MIN = 0;

static const s8 S8_MAX = 0x7f;
static const s8 S8_MIN = -S8_MAX - 1;

typedef unsigned short u16;
typedef signed short s16;

static const u16 U16_MAX = 0xffff;
static const u16 U16_MIN = 0;

static const s16 S16_MAX = 0x7fff;
static const s16 S16_MIN = -S16_MAX - 1;

typedef unsigned int u32;
typedef signed int s32;

static const u32 U32_MAX = 0xffffffff;
static const u32 U32_MIN = 0;

static const s32 S32_MAX = 0x7fffffff;
static const s32 S32_MIN = -S32_MAX - 1;

typedef unsigned long long u64;
typedef signed long long s64;

static const u64 U64_MAX = 0xffffffffffffffff;
static const u64 U64_MIN = 0;

static const s64 S64_MAX = 0x7fffffffffffffff;
static const s64 S64_MIN = -S64_MAX - 1;

// Unsigned and signed integer types with the width of the general purpose
// registers of the CPU.

#define XLEN_BITS (sizeof(long) * 8)

typedef unsigned long ulen;
typedef signed long slen;

static const ulen ULEN_MAX = (ulen) -1;
static const ulen ULEN_MIN = 0;

static const slen SLEN_MAX = ULEN_MAX / 2;
static const slen SLEN_MIN = -SLEN_MAX - 1;

// Shorthand for the string type. Note that strings always treated as
// null-terminated and immutable.
typedef const char *str;

// Optional types that must be used instead of `NULL` which isn't provided.
//
// # Fields
//
// * `value` - Some useful value of the given type. If accessed while it's not
//   present, the behavior is undefined.
// * `present` - Whether the `value` is present.

#define Maybe(type) struct {type value; bool present;}
#define Just(v) {.value = (v), .present = true}
#define Nothing {0}

typedef Maybe(ulen) MaybeUlen;
typedef Maybe(slen) MaybeSlen;

// Indicates that when this point in code is reached, the execution won't
// progress anymore. Does nothing forever.
static inline noreturn void hang(void) {
    for (;;) {}
}

// Indicates that this point in code must never be reached. Triggers undefined
// behavior by dereferencing a wild pointer.
static inline noreturn void crash(void) {
    volatile u8 *nowhere = (void *) ULEN_MAX;

    *nowhere = 0;

    hang();
}

#endif
