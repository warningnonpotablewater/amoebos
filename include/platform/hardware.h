#ifndef PLATFORM_HARDWARE_H
#define PLATFORM_HARDWARE_H

#include <runtime/primitives.h>

struct _display {
    volatile u8 *buffer;
    u32 size;
    u16 width;
    u16 height;
    u16 frame_rate;
};

// Points to the framebuffer which pixels can be directly manipulated in order
// to display an image.
static const struct _display hardware_display = {
    .buffer = (void *) 0x01000000,
    .size = 240 * 400,
    .width = 240,
    .height = 400,
    .frame_rate = 30
};

#define HARDWARE_DISPLAY_OFFSET(x, y)\
    ((ulen) (y) * hardware_display.width + (ulen) (x))

struct _touch {
    u16 type;
    u16 x;
    u16 y;
};

typedef const volatile struct _touch *_touch;

// Points to data of the current touch event. If accessed outside of an
// `event_handler`, the contents are undefined.
static const _touch hardware_touch = (void *) 0x02000000;

enum {
    HARDWARE_TOUCH_DOWN = 0,
    HARDWARE_TOUCH_MOVE = 1,
    HARDWARE_TOUCH_UP = 2
};

struct _sound {
    volatile u8 *buffer;
    u16 size;
    u16 sample_rate;
};

// Points to the audio buffer that contains enough space to fit samples for one
// video frame. Should be accessed only during a frame sync event.
static const struct _sound hardware_sound = {
    .buffer = (void *) 0x03000000,
    .size = 1024,
    .sample_rate = 30720,
};

#endif
