#ifndef PLATFORM_ABI_H
#define PLATFORM_ABI_H

// This header file is used by both C and assembly and should only contain
// preprocessor macros.

#define ECALL_SYSTEM_DEBUG 0
#define ECALL_EVENT_SET_HANDLER 1
#define ECALL_EVENT_REMOVE_HANDLER 2
#define ECALL_EVENT_RETURN 3
#define ECALL_FILE_OPEN 4
#define ECALL_FILE_EXECUTE 5

#define EVENT_FRAME_SYNC 0
#define EVENT_TOUCH_INPUT 1

#endif
