# This project is no longer developed

I don't want to ever program anything in C again.

---

amoebos is a very WIP operating system for [BlastMachine One].

[BlastMachine One]: https://gitlab.com/warningnonpotablewater/bm1

## Dependencies

* [Meson]
* [RISC-V GNU toolchain]

[Meson]: https://mesonbuild.com/
[RISC-V GNU toolchain]: https://github.com/riscv-collab/riscv-gnu-toolchain

## How to build

```sh
git submodule update --init --recursive
meson setup build --cross-file riscv32-bm1-elf.txt
meson compile -C build
```

## Documentation

Public API interfaces are documented in the header files of their
respective libraries:

* [libruntime] - provides an unapologetically incompatible alternative
  to the C standard library.
* [libuser] - lets userspace programs talk to the kernel.
* [libplatform] - abstracts the hardware of BlastMachine One.

[libruntime]: include/runtime
[libuser]: include/user
[libplatform]: include/platform

## Portability

Most of the kernel and userspace code is heavily tailored to RISC-V
and BlastMachine One specifically, and as such, is not portable.
libruntime, on the other hand, uses abstractions wherever possible and
should be portable to some extent. It currently makes the following
assumptions about the platform it's running on:

* Bytes are 8 bits wide.
* Integers have no padding bits and no trap representations.
* Signed integers use a two's complement representation.
* Casting an unsigned integer to a signed integer of the same size
  results in a bit cast.
* Right-shifting a signed integer results in an arithmetic shift.
* Pointers are at least 32 bits wide and can be treated as integers.
* General purpose registers are at least as wide as a pointer.

## Development guidelines

* Write in C11 and avoid GNU extensions whenever possible.
* Don't use inline assembly.
* Don't use dynamic memory allocation.
* Don't use floating point math.
* Have fun :)

## Meaning behind the name

amoeba + amogus = amoebos.

## License

Copyright 2023 Warning: Non-Potable Water

[GNU LGPL v3 or later](LICENSE).
