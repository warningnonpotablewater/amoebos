#ifndef CSR_H
#define CSR_H

#include <runtime/primitives.h>

ulen csr_read_mepc(void);
ulen csr_read_mcause(void);

void csr_enable_mpie(void);
void csr_disable_mpie(void);

void csr_jump_user(void *);
void csr_return_user(void);

#endif
