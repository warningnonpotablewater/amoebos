#include "trap.h"

noreturn void main(void) {
    trap_setup_vector();
    trap_enter_userspace();

    crash();
}
