// This module implements its own rendering routines that are meant to survive
// even if the rest of the system doesn't, and should be kept dependency-free.

#include <platform/hardware.h>

#include "csr.h"
#include "debug.h"
#include "star-platinum.h"

#define GLYPH_SIZE 8

static void fill_screen(u8 color) {
    for (ulen i = 0; i < hardware_display.size; i++) {
        hardware_display.buffer[i] = color;
    }
}

static void print_char(s16 x, s16 y, u8 color, u8 chr) {
    bool out_of_bounds =
        x >= hardware_display.width ||
        y >= hardware_display.height ||
        x <= -GLYPH_SIZE ||
        y <= -GLYPH_SIZE;

    bool non_ascii = chr > 127;

    if (out_of_bounds || non_ascii) {
        return;
    }

    u8 *glyph = sp_font_data[chr];

    for (u8 row = 0; row < GLYPH_SIZE; row++) {
        u8 byte = glyph[row];

        for (u8 column = 0; column < GLYPH_SIZE; column++) {
            u8 bit_index = GLYPH_SIZE - 1 - column;
            u8 bit = (byte >> bit_index) & 1;

            slen display_x = x + column;
            slen display_y = y + row;

            out_of_bounds =
                display_x < 0 ||
                display_y < 0 ||
                display_x >= hardware_display.width ||
                display_y >= hardware_display.height;

            if (!bit || out_of_bounds) {
                continue;
            }

            ulen offset = HARDWARE_DISPLAY_OFFSET(display_x, display_y);

            hardware_display.buffer[offset] = color;
        }
    }
}

static void print_number(s16 x, s16 y, u8 color, ulen number) {
    str digits = "0123456789abcdef";

    for (slen offset = XLEN_BITS - 4; offset >= 0; offset -= 4) {
        ulen nibble = (number >> offset) & 0xf;

        print_char(x, y, color, digits[nibble]);

        x += GLYPH_SIZE;
    }
}

void debug(s16 x, s16 y, u8 color, str message) {
    s16 origin_x = x;

    for (const char *chr = message; *chr != 0; chr++) {
        bool line_break = *chr == '\n';
        bool overflow = x >= hardware_display.width;

        if (line_break || overflow) {
            y += GLYPH_SIZE;
            x = origin_x;
        }

        if (line_break) {
            continue;
        }

        print_char(x, y, color, *chr);

        x += GLYPH_SIZE;
    }
}

noreturn void panic(void) {
    fill_screen(0x00);

    debug(8, 8, 0xff,
        "FATAL SOFTWARE ERROR\n"
        "--------------------\n\n"
        "Your device needs to be\n"
        "restarted. Turn it off or\n"
        "press the reset button.\n\n"
        "Guru meditation:\n\n"
        "mepc =\n"
        "mcause ="
    );

    print_number(80, 80, 0xff, csr_read_mepc());
    print_number(80, 88, 0xff, csr_read_mcause());

    hang();
}
