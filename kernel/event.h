#ifndef EVENT_H
#define EVENT_H

#include <runtime/primitives.h>

void event_set_handler(void *);
void event_remove_handler(void);
void event_enter(ulen);
void event_return(void);

#endif
