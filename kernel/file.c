// Implements the Unix System V ar (archiver) format. Massively work in
// progress.

#include <runtime/array.h>
#include <runtime/ascii.h>
#include <runtime/memory.h>
#include <runtime/string.h>

#include "csr.h"
#include "event.h"
#include "file.h"
#include "sram.h"

struct archive_header {
    u8 magic[8];
};

struct file_header {
    u8 name[16];
    u8 timestamp[12];
    u8 owner[6];
    u8 group[6];
    u8 mode[8];
    u8 size[10];
    u8 magic[2];
};

str ARCHIVE_MAGIC = "!<arch>\n";
str FILE_MAGIC = "`\n";

const ulen HEADER_ALIGNMENT = 2;
const u8 FILENAME_TERMINATOR = '/';

static slen is_digit(const void *c, const void *unused) {
    (void) unused;

    const u8 *character = c;

    return !ascii_is_decimal(*character);
}

bool file_open(struct file_handle *handle, str name) {
    u8 *storage = (void *) SRAM_STORAGE;
    struct archive_header *archive = (void *) storage;

    bool not_an_archive = memory_compare(
        archive->magic,
        ARCHIVE_MAGIC,
        sizeof(archive->magic)
    ) != 0;

    if (not_an_archive) {
        return false;
    }

    storage += sizeof(struct archive_header);

    ulen name_length = string_length(name);

    while (storage < (u8 *) SRAM_END) {
        struct file_header *file = (void *) storage;

        bool not_a_file = memory_compare(
            file->magic,
            FILE_MAGIC,
            sizeof(file->magic)
        );

        if (not_a_file) {
            return false;
        }

        MaybeUlen filename_length = memory_find_byte(
            file->name,
            sizeof(file->name),
            FILENAME_TERMINATOR
        );

        if (!filename_length.present) {
            return false;
        }

        bool filename_matches =
            filename_length.value == name_length
            && memory_compare(file->name, name, name_length) == 0;

        MaybeUlen file_size_last_digit = array_find_last(
            file->size,
            file->size, // Unused.
            sizeof(u8),
            sizeof(file->size),
            is_digit
        );

        if (!file_size_last_digit.present) {
            return false;
        }

        MaybeUlen file_size = ascii_parse_ulen(
            file->size,
            file_size_last_digit.value + 1,
            10
        );

        if (!file_size.present) {
            return false;
        }

        storage += sizeof(struct file_header);

        if (filename_matches) {
            handle->data = storage;
            handle->size = file_size.value;

            return true;
        } else {
            storage += file_size.value;
            ulen alignment = (ulen) storage % HEADER_ALIGNMENT;

            if (alignment != 0) {
                storage += HEADER_ALIGNMENT - alignment;
            }

            continue;
        }
    }

    return false;
}

void file_execute(str name) {
    struct file_handle file = {0};
    bool success = file_open(&file, name);

    if (!success) {
        return;
    }

    memory_copy(file.data, (void *) SRAM_USERSPACE, file.size);

    // The function may be called in an event_handler.

    csr_enable_mpie();
    event_remove_handler();

    csr_jump_user((void *) SRAM_USERSPACE);
}
