.global csr_read_mepc
.global csr_read_mcause

.global csr_enable_mpie
.global csr_disable_mpie

.global csr_jump_user
.global csr_return_user

csr_read_mepc:
    csrr a0, mepc

    ret

csr_read_mcause:
    csrr a0, mcause

    ret

csr_enable_mpie:
    li t0, 0x80

    csrs mstatus, t0 // mpie = 1.

    ret

csr_disable_mpie:
    li t0, 0x80

    csrc mstatus, t0 // mpie = 0.

    ret

csr_jump_user:
    csrr t0, mepc
    sw t0, old_pc, t1

    csrw mepc, a0 // Provided by the caller.

    ret

csr_return_user:
    lw t0, old_pc
    csrw mepc, t0

    ret

.data

old_pc: .4byte 0
