#ifndef TRAP_H
#define TRAP_H

#include <runtime/primitives.h>

enum {
    EXCEPTION_INSTRUCTION_ADDRESS_MISALIGNED = 0,
    EXCEPTION_INSTRUCTION_ACCESS_FAULT = 1,
    EXCEPTION_ILLEGAL_INSTRUCTION = 2,
    EXCEPTION_BREAKPOINT = 3,
    EXCEPTION_LOAD_ADDRESS_MISALIGNED = 4,
    EXCEPTION_LOAD_ACCESS_FAULT = 5,
    EXCEPTION_STORE_ADDRESS_MISALIGNED = 6,
    EXCEPTION_STORE_ACCESS_FAULT = 7,
    EXCEPTION_ECALL = 11,

    INTERRUPT_FRAME_SYNC = -1,
    INTERRUPT_TOUCH_INPUT = -2
};

struct registers {
    ulen user_x[31]; // No x0.
    ulen kernel_gp;
    ulen kernel_sp;
};

extern struct registers trap_registers;

void trap_handle(slen);

void trap_setup_vector(void);
noreturn void trap_enter_userspace(void);
void trap_skip_instruction(void);

#endif
