#include <runtime/memory.h>

#include "csr.h"
#include "event.h"
#include "trap.h"

static ulen event_x[31] = {0};

static bool has_handler = false;
static void *handler;

void event_set_handler(void *callback) {
    has_handler = true;
    handler = callback;
}

void event_remove_handler(void) {
    has_handler = false;
}

void event_enter(ulen type) {
    if (!has_handler) {
        return;
    }

    memory_copy(trap_registers.user_x, event_x, sizeof(event_x));

    trap_registers.user_x[9] = type; // a0.

    csr_disable_mpie();
    csr_jump_user(handler);
}

void event_return(void) {
    memory_copy(event_x, trap_registers.user_x, sizeof(event_x));

    csr_enable_mpie();
    csr_return_user();
}
