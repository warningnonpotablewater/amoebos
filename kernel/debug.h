#ifndef DEBUG_H
#define DEBUG_H

#include <runtime/primitives.h>

void debug(s16, s16, u8, str);
noreturn void panic(void);

#endif
