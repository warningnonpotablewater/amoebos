#ifndef SRAM_H
#define SRAM_H

// This header file is used by both C and assembly and should only contain
// preprocessor macros.

#define SRAM_KERNEL 0x80000000
#define SRAM_USERSPACE 0x80200000
#define SRAM_STORAGE 0x80400000
#define SRAM_END 0x807fffff

#endif
