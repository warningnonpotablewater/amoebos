#ifndef FILE_H
#define FILE_H

#include <runtime/primitives.h>

struct file_handle {
    const u8 *data;
    ulen size;
};

bool file_open(struct file_handle *, str);
void file_execute(str);

#endif
