#include <platform/abi.h>

#include "debug.h"
#include "event.h"
#include "file.h"
#include "sram.h"
#include "trap.h"

struct registers trap_registers = {0};

static void handle_ecall() {
    ulen type = trap_registers.user_x[4]; // t0.
    ulen *args = &trap_registers.user_x[9]; // a0.

    bool success = false;

    switch (type) {
    case ECALL_SYSTEM_DEBUG:
        debug((s16) args[0], (s16) args[1], (u8) args[2], (void *) args[3]);

        break;
    case ECALL_EVENT_SET_HANDLER:
        event_set_handler((void *) args[0]);

        break;
    case ECALL_EVENT_REMOVE_HANDLER:
        event_remove_handler();

        break;
    case ECALL_EVENT_RETURN:
        event_return();

        break;
    case ECALL_FILE_OPEN:
        success = file_open((void *) args[0], (void *) args[1]);

        args[0] = success;

        break;
    case ECALL_FILE_EXECUTE:
        file_execute((void *) args[0]);

        break;
    default:
        panic();
    }
}

void trap_handle(slen cause) {
    switch (cause) {
    case EXCEPTION_ECALL:
        trap_skip_instruction();
        handle_ecall();

        break;
    case INTERRUPT_FRAME_SYNC:
        event_enter(EVENT_FRAME_SYNC);

        break;
    case INTERRUPT_TOUCH_INPUT:
        event_enter(EVENT_TOUCH_INPUT);

        break;
    default:
        panic();
    }
}
