#include <runtime/array.h>
#include <runtime/format.h>
#include <runtime/integer.h>
#include <runtime/memory.h>
#include <runtime/print.h>
#include <runtime/random.h>
#include <runtime/string.h>

#include <user/event.h>
#include <user/file.h>
#include <user/system.h>

#include <platform/hardware.h>

void handle_event(ulen type) {
    if (type != EVENT_TOUCH_INPUT) {
        goto finish;
    }

    u16 x = hardware_touch->x - 4;
    u16 y = hardware_touch->y - 4;

    u8 color = 0;

    switch (hardware_touch->type) {
    case HARDWARE_TOUCH_DOWN:
        color = 0x55;

        break;
    case HARDWARE_TOUCH_MOVE:
        color = 0xaa;

        break;
    case HARDWARE_TOUCH_UP:
        file_execute("test.com");

        crash();
    }

    system_debug((s16) x, (s16) y, color, "\x7f");

finish:
    event_return();
}

slen compare_bytes(const void *a, const void *b) {
    const u8 *left = a;
    const u8 *right = b;

    return *left - *right;
}

void main(void) {
    memory_fill(
        (void *) hardware_display.buffer,
        hardware_display.size,
        0xff
    );

    u8 data[256] = {0};

    PRINT(data, "%u %u %i %i", ULEN_MIN, ULEN_MAX, SLEN_MIN, SLEN_MAX);

    system_debug(8, 8, 0x00, (str) data);

    event_set_handler(handle_event);
}
