#include <runtime/random.h>

#include "test.h"

static const ulen ITERATIONS = 1000;

void test_random(void) {
    struct random_state state = random_new(0);

    bool in_range = true;

    for (ulen i = 0; i < ITERATIONS; i++) {
        u32 number = random_u32(&state, 100, 600);

        if (number < 100 || number > 600) {
            in_range = false;

            break;
        }
    }

    test(
        "Unsigned random numbers stay in range",
        in_range
    );

    in_range = true;

    for (ulen i = 0; i < ITERATIONS; i++) {
        s32 number = random_s32(&state, -250, 250);

        if (number < -250 || number > 250) {
            in_range = false;

            break;
        }
    }

    test(
        "Signed random numbers stay in range",
        in_range
    );
}
