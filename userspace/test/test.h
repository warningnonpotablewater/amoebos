#ifndef TEST_H
#define TEST_H

#include <runtime/primitives.h>

void test(str, bool);

void test_ascii(void);
void test_string(void);
void test_random(void);
void test_integer(void);
void test_memory(void);

#endif
