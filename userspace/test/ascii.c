#include <runtime/ascii.h>

#include "test.h"

void test_is_non_ascii(void) {
    test(
        "0x80 is a non-ASCII character",
        ascii_is_non_ascii(0x80)
    );
    test(
        "0xAA is a non-ASCII character",
        ascii_is_non_ascii(0xaa)
    );
    test(
        "0xFF is a non-ASCII character",
        ascii_is_non_ascii(0xff)
    );

    test(
        "A control character is not a non-ASCII character",
        !ascii_is_non_ascii('\0')
    );
    test(
        "A whitespace character is not a non-ASCII character",
        !ascii_is_non_ascii(' ')
    );
    test(
        "A lowercase letter is not a non-ASCII character",
        !ascii_is_non_ascii('a')
    );
    test(
        "An uppercase letter is not a non-ASCII character",
        !ascii_is_non_ascii('A')
    );
    test(
        "A decimal digit is not a non-ASCII character",
        !ascii_is_non_ascii('0')
    );
    test(
        "A symbol is not a non-ASCII character",
        !ascii_is_non_ascii('!')
    );
}

void test_is_control(void) {
    test(
        "Null is a control character",
        ascii_is_control('\0')
    );
    test(
        "Tab is a control character",
        ascii_is_control('\t')
    );
    test(
        "Line feed is a control character",
        ascii_is_control('\n')
    );
    test(
        "Unit separator is a control character",
        ascii_is_control(0x1f)
    );
    test(
        "Delete is a control character",
        ascii_is_control(0x7f)
    );

    test(
        "A non-ASCII character is not a control character",
        !ascii_is_control(0xff)
    );
    test(
        "Space is not a control character",
        !ascii_is_control(' ')
    );
    test(
        "A lowercase letter is not a control character",
        !ascii_is_control('a')
    );
    test(
        "An uppercase letter is not a control character",
        !ascii_is_control('A')
    );
    test(
        "A decimal digit is not a control character",
        !ascii_is_control('0')
    );
    test(
        "A symbol is not a control character",
        !ascii_is_control('!')
    );
}

void test_is_whitespace(void) {
    test(
        "Tab is a whitespace character",
        ascii_is_whitespace('\t')
    );
    test(
        "Line feed is a whitespace character",
        ascii_is_whitespace('\n')
    );
    test(
        "Vertical tab is a whitespace character",
        ascii_is_whitespace('\v')
    );
    test(
        "Carriage return is a whitespace character",
        ascii_is_whitespace('\r')
    );
    test(
        "Space is a whitespace character",
        ascii_is_whitespace(' ')
    );

    test(
        "A non-ASCII character is not a whitespace character",
        !ascii_is_whitespace(0xff)
    );
    test(
        "Null is not a whitespace character",
        !ascii_is_whitespace('\0')
    );
    test(
        "A lowercase letter is not a whitespace character",
        !ascii_is_whitespace('a')
    );
    test(
        "An uppercase letter is not a whitespace character",
        !ascii_is_whitespace('A')
    );
    test(
        "A decimal digit is not a whitespace character",
        !ascii_is_whitespace('0')
    );
    test(
        "A symbol is not a whitespace character",
        !ascii_is_whitespace('!')
    );
}

void test_is_lowercase(void) {
    test(
        "'a' is a lowercase letter",
        ascii_is_lowercase('a')
    );
    test(
        "'h' is a lowercase letter",
        ascii_is_lowercase('h')
    );
    test(
        "'z' is a lowercase letter",
        ascii_is_lowercase('z')
    );

    test(
        "A non-ASCII character is not a lowercase letter",
        !ascii_is_lowercase(0xff)
    );
    test(
        "A control character is not a lowercase letter",
        !ascii_is_lowercase('\0')
    );
    test(
        "A whitespace character is not a lowercase letter",
        !ascii_is_lowercase(' ')
    );
    test(
        "An uppercase letter is not a lowercase letter",
        !ascii_is_lowercase('A')
    );
    test(
        "A decimal digit is not a lowercase letter",
        !ascii_is_lowercase('0')
    );
    test(
        "A symbol is not a lowercase letter",
        !ascii_is_lowercase('!')
    );
}

void test_is_uppercase(void) {
    test(
        "'A' is an uppercase letter",
        ascii_is_uppercase('A')
    );
    test(
        "'H' is an uppercase letter",
        ascii_is_uppercase('H')
    );
    test(
        "'Z' is an uppercase letter",
        ascii_is_uppercase('Z')
    );

    test(
        "A non-ASCII character is not an uppercase letter",
        !ascii_is_uppercase(0xff)
    );
    test(
        "A control character is not an uppercase letter",
        !ascii_is_uppercase('\0')
    );
    test(
        "A whitespace character is not an uppercase letter",
        !ascii_is_uppercase(' ')
    );
    test(
        "A lowercase letter is not an uppercase letter",
        !ascii_is_uppercase('a')
    );
    test(
        "A decimal digit is not an uppercase letter",
        !ascii_is_uppercase('0')
    );
    test(
        "A symbol is not an uppercase letter",
        !ascii_is_uppercase('!')
    );
}

void test_is_decimal(void) {
    test(
        "'0' is a decimal digit",
        ascii_is_decimal('0')
    );
    test(
        "'6' is a decimal digit",
        ascii_is_decimal('6')
    );
    test(
        "'9' is a decimal digit",
        ascii_is_decimal('9')
    );

    test(
        "A non-ASCII character is not a decimal digit",
        !ascii_is_decimal(0xff)
    );
    test(
        "A control character is not a decimal digit",
        !ascii_is_decimal('\0')
    );
    test(
        "A whitespace character is not a decimal digit",
        !ascii_is_decimal(' ')
    );
    test(
        "A lowercase letter is not a decimal digit",
        !ascii_is_decimal('a')
    );
    test(
        "An uppercase letter is not a decimal digit",
        !ascii_is_decimal('A')
    );
    test(
        "A symbol is not a decimal digit",
        !ascii_is_decimal('!')
    );
}

void test_is_symbol(void) {
    test(
        "'!' is a symbol",
        ascii_is_symbol('!')
    );
    test(
        "'&' is a symbol",
        ascii_is_symbol('&')
    );
    test(
        "'/' is a symbol",
        ascii_is_symbol('/')
    );

    test(
        "':' is a symbol",
        ascii_is_symbol(':')
    );
    test(
        "'?' is a symbol",
        ascii_is_symbol('?')
    );
    test(
        "'@' is a symbol",
        ascii_is_symbol('@')
    );

    test(
        "'[' is a symbol",
        ascii_is_symbol('[')
    );
    test(
        "'_' is a symbol",
        ascii_is_symbol('_')
    );
    test(
        "'`' is a symbol",
        ascii_is_symbol('`')
    );

    test(
        "'{' is a symbol",
        ascii_is_symbol('{')
    );
    test(
        "'|' is a symbol",
        ascii_is_symbol('|')
    );
    test(
        "'~' is a symbol",
        ascii_is_symbol('~')
    );

    test(
        "A non-ASCII character is not a symbol",
        !ascii_is_symbol(0xff)
    );
    test(
        "A control character is not a symbol",
        !ascii_is_symbol('\0')
    );
    test(
        "A whitespace character is not a symbol",
        !ascii_is_symbol(' ')
    );
    test(
        "A lowercase letter is not a symbol",
        !ascii_is_symbol('a')
    );
    test(
        "An uppercase letter is not a symbol",
        !ascii_is_symbol('A')
    );
    test(
        "A decimal digit is not a symbol",
        !ascii_is_symbol('0')
    );
}

static void test_to_lowercase(void) {
    u8 character = 0;

    character = 'A';
    ascii_to_lowercase(&character, sizeof(u8));

    test(
        "ascii_to_lowercase makes uppercase letters lowercase",
        character == 'a'
    );

    character = 'h';
    ascii_to_lowercase(&character, sizeof(u8));

    test(
        "ascii_to_lowercase doesn't touch lowercase letters",
        character == 'h'
    );

    character = '0';
    ascii_to_lowercase(&character, sizeof(u8));

    test(
        "ascii_to_lowercase doesn't touch non-letter characters",
        character == '0'
    );
}

static void test_to_uppercase(void) {
    u8 character = 0;

    character = 'a';
    ascii_to_uppercase(&character, sizeof(u8));

    test(
        "ascii_to_uppercase makes lowercase letters uppercase",
        character == 'A'
    );

    character = 'H';
    ascii_to_uppercase(&character, sizeof(u8));

    test(
        "ascii_to_uppercase doesn't touch uppercase letters",
        character == 'H'
    );

    character = '0';
    ascii_to_uppercase(&character, sizeof(u8));

    test(
        "ascii_to_uppercase doesn't touch non-letter characters",
        character == '0'
    );
}

void test_ascii(void) {
    test_is_non_ascii();
    test_is_control();
    test_is_whitespace();
    test_is_lowercase();
    test_is_uppercase();
    test_is_decimal();
    test_is_symbol();
    test_to_lowercase();
    test_to_uppercase();
}
