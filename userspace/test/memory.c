#include <runtime/memory.h>

#include "test.h"

static void test_corruptions(void) {
    u32 copy_from[3] = {2011, 10, 24};
    u32 copy_to[3] = {0};

    memory_copy(copy_from, copy_to, sizeof(copy_from));

    test(
        "Copying memory regions doesn't corrupt them",
        (
            copy_from[0] == copy_to[0]
            && copy_from[1] == copy_to[1]
            && copy_from[2] == copy_to[2]
        )
    );

    u32 overlapping[5] = {5, 6, 7, 9};

    memory_copy(&overlapping[0], &overlapping[1], 3 * sizeof(u32));

    test(
        "Copying overlapping memory regions doesn't corrupt them",
        (
            overlapping[0] == 5
            && overlapping[1] == 5
            && overlapping[2] == 6
            && overlapping[3] == 7
        )
    );

    memory_copy(0, 0, 0);

    test(
        "Copying 0 bytes doesn't invoke undefined behavior",
        true
    );

    u32 to_fill[3] = {0};

    memory_fill(to_fill, sizeof(to_fill), 0x69);

    test(
        "Filling memory regions doesn't corrupt them",
        (
            to_fill[0] == 0x69696969
            && to_fill[1] == 0x69696969
            && to_fill[2] == 0x69696969
        )
    );

    memory_fill(0, 0, 0);

    test(
        "Filling 0 bytes doesn't invoke undefined behavior",
        true
    );

    u32 to_swap[3] = {3, 2, 1};

    memory_swap(&to_swap[0], &to_swap[2], sizeof(u32));

    test(
        "Swapping memory regions doesn't corrupt them",
        to_swap[0] == 1 && to_swap[1] == 2 && to_swap[2] == 3
    );

    memory_swap(0, 0, 0);

    test(
        "Swapping 0 bytes doesn't invoke undefined behavior",
        true
    );
}

static void test_compare(void) {
    u32 ascending[4] = {1, 2, 3, 4};
    u32 ascending2[4] = {1, 2, 3, 4};
    u32 descending[4] = {4, 3, 2, 1};

    test(
        "An empty memory region is equal to itself",
        memory_compare(ascending, descending, 0) == 0
    );

    test(
        "A memory region is equal to itself",
        memory_compare(ascending, ascending, sizeof(ascending)) == 0
    );

    test(
        "A memory region is equal to a region with identical bytes",
        memory_compare(ascending, ascending2, sizeof(ascending)) == 0
    );

    test(
        "An ascending memory region < a descending memory region",
        memory_compare(ascending, descending, sizeof(ascending)) < 0
    );

    test(
        "A descending memory region > an ascending memory region",
        memory_compare(descending, ascending, sizeof(descending)) > 0
    );
}

static void test_find_byte(void) {
    u8 bytes[4] = {4, 99, 6, 52};

    MaybeUlen found = memory_find_byte(bytes, sizeof(bytes), 99);

    test(
        "A byte present in the memory region can be found",
        found.present && found.value == 1
    );

    test(
        "A byte not present in the memory region can't be found",
        !memory_find_byte(bytes, sizeof(bytes), 0).present
    );

    test(
        "A byte can't be found in an empty memory region",
        !memory_find_byte(bytes, 0, 4).present
    );
}

static void test_find_memory(void) {
    u32 haystack[5] = {1, 69, 420, 4, 5};
    u32 needle[2] = {69, 420};
    u32 bad_needle[2] = {420, 69};

    MaybeUlen found = memory_find_memory(
        haystack, sizeof(haystack),
        needle, sizeof(needle)
    );

    test(
        "A byte sequence present in the memory region can be found",
        found.present && found.value == 1 * sizeof(u32)
    );

    MaybeUlen nothing = memory_find_memory(
        haystack, sizeof(haystack),
        needle, 0
    );

    test(
        "An empty byte sequence can be found in a memory region",
        nothing.present && nothing.value == 0
    );

    MaybeUlen itself = memory_find_memory(
        haystack, sizeof(haystack),
        haystack, sizeof(needle)
    );

    test(
        "A subset of a memory region can be found in itself",
        itself.present && itself.value == 0
    );

    test(
        "A subset of a memory region can't be bigger than the region itself",
        !memory_find_memory(
            haystack, sizeof(needle),
            haystack, sizeof(haystack)
        ).present
    );

    test(
        "A byte sequence not present in the memory region can't be found",
        !memory_find_memory(
            haystack, sizeof(haystack),
            bad_needle, sizeof(bad_needle)
        ).present
    );

    test(
        "A byte sequence can't be found in an empty memory region",
        !memory_find_memory(haystack, 0, needle, sizeof(needle)).present
    );

    test(
        "A byte sequence can't be found in a smaller memory region",
        !memory_find_memory(
            haystack, sizeof(needle),
            needle, sizeof(haystack)
        ).present
    );
}

void test_memory(void) {
    test_corruptions();
    test_compare();
    test_find_byte();
    test_find_memory();
}
