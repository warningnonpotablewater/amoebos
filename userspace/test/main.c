#include <runtime/memory.h>
#include <runtime/primitives.h>
#include <runtime/print.h>

#include <user/system.h>

#include <platform/hardware.h>

#include "test.h"

static ulen tests_passed = 0;

void main(void) {
    memory_fill(
        (void *) hardware_display.buffer,
        hardware_display.size,
        0xff
    );

    test_ascii();
    test_string();
    test_random();
    test_integer();
    test_memory();

    u8 message[256] = {0};
    PRINT(message, "Tests passed: %u", tests_passed);

    system_debug(8, 8, 0x00, (str) message);
}

void test(str test_case, bool passed) {
    if (!passed) {
        u8 message[256] = {0};
        PRINT(message, "FAIL: %s", test_case);

        system_debug(8, 8, 0x00, (str) message);

        hang();
    }

    tests_passed++;
}
