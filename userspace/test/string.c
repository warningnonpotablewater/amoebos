#include <runtime/string.h>

#include "test.h"

static void test_length(void) {
    test(
        "An empty string has the length of 0",
        string_length("") == 0
    );

    test(
        "A string with one character has the length of 1",
        string_length("h") == 1
    );

    test(
        "A string ends with a null even if there are characters after it",
        string_length("h\0iiiiiiiiiiiiiii") == 1
    );

    test(
        "ASCII characters count when measuring a string's length",
        string_length("Capybara") == 8
    );

    test(
        "Non-ASCII characters count when measuring a string's length",
        string_length("C\x80pybar\xff") == 8
    );
}

static void test_compare(void) {
    test(
        "Two empty strings are equal",
        string_compare("", "") == 0
    );

    test(
        "Two identical non-empty strings are equal",
        string_compare("resonance", "resonance") == 0
    );

    test(
        "An empty and a non-empty strings are not equal",
        string_compare("", "h") < 0
    );

    test(
        "A string with the greatest length is the longest",
        string_compare("eee", "eeeeee") < 0
    );

    test(
        "A string with the greatest differing codepoint is the longest",
        string_compare("   b????", "   a????") > 0
    );

    test(
        "A string with the smallest differing codepoint is the shortest",
        string_compare("   a!!!!", "   b!!!!") < 0
    );

    test(
        "Non-ASCII characters count when comparing strings",
        string_compare("C\x80pybar\xff", "C\x80pybar\xff") == 0
    );
}

static void test_find_byte(void) {
    test(
        "A byte can't be found in an empty string",
        !string_find_byte("", 'L').present
    );

    test(
        "A byte that is not in the string can't be found in it",
        !string_find_byte("Win", 'L').present
    );

    MaybeUlen byte = string_find_byte("Catalina", 'a');

    test(
        "When a string has more than one identical byte, the first is found",
        byte.present && byte.value == 1
    );

    MaybeUlen non_ascii = string_find_byte("What th\xff", 0xff);

    test(
        "A non-ASCII byte can be found in a string if it's in it",
        non_ascii.present && non_ascii.value == 7
    );
}

static void test_find_string(void) {
    test(
        "A string can't be found in an empty string",
        !string_find_string("", "something").present
    );

    test(
        "A string that is not in the string can't be found in it",
        !string_find_string("New York City", "Me").present
    );

    test(
        "A string can't be found in a shorter string",
        !string_find_string("Life", "Meaning").present
    );

    MaybeUlen string = string_find_string("humuhumunukunukuapua'a", "umu");

    test(
        "When a string has more than one identical string, the first is found",
        string.present && string.value == 1
    );

    MaybeUlen non_ascii = string_find_string("my fav bepi\x80", "bepi\x80");

    test(
        "A string with a non-ASCII byte can be found in a string if it's in it",
        non_ascii.present && non_ascii.value == 7
    );
}

static void test_parse_ulen(void) {
    MaybeUlen base10 = string_parse_ulen("1989", 10);

    test(
        "Positive base 10 numbers can be parsed as unsigned",
        base10.present && base10.value == 1989
    );

    MaybeUlen non_base10 = string_parse_ulen("1989", 11);

    test(
        "Positive non-base 10 numbers can be parsed as unsigned",
        non_base10.present && non_base10.value == 2517
    );

    test(
        "Negative numbers can't be parsed an unsigned",
        !string_parse_ulen("-1989", 10).present
    );

    test(
        "Numbers greater than ULEN_MAX can't be parsed as unsigned",
        !string_parse_ulen("99999999999999999999999999999999999999", 10).present
    );

    test(
        "Non-integers can't be parsed as unsigned",
        !string_parse_ulen("1989.99", 10).present
    );

    test(
        "Numbers with trailing characters can't be parsed as unsigned",
        !string_parse_ulen("1989 ", 10).present
    );

    test(
        "Numbers with a base lower than 2 can't be parsed an unsigned",
        !string_parse_ulen("1989", 1).present
    );

    test(
        "Numbers with a base greater than 36 can't be parsed an unsigned",
        !string_parse_ulen("1989", 37).present
    );
}

static void test_parse_slen(void) {
    MaybeSlen positive_base10 = string_parse_slen("1989", 10);

    test(
        "Positive base 10 numbers can be parsed as signed",
        positive_base10.present && positive_base10.value == 1989
    );

    MaybeSlen negative_base10 = string_parse_slen("-1989", 10);

    test(
        "Negative base 10 numbers can be parsed as signed",
        negative_base10.present && negative_base10.value == -1989
    );

    MaybeSlen positive_non_base10 = string_parse_slen("1989", 11);

    test(
        "Positive non-base 10 numbers can be parsed as signed",
        positive_non_base10.present && positive_non_base10.value == 2517
    );

    MaybeSlen negative_non_base10 = string_parse_slen("-1989", 11);

    test(
        "Negative non-base 10 numbers can be parsed as signed",
        negative_non_base10.present && negative_non_base10.value == -2517
    );

    test(
        "Numbers smaller than SLEN_MIN can't be parsed as signed",
        !string_parse_slen("-9999999999999999999999999999999999999", 10).present
    );

    test(
        "Numbers greater than SLEN_MAX can't be parsed as signed",
        !string_parse_slen("99999999999999999999999999999999999999", 10).present
    );

    test(
        "Non-integers can't be parsed as signed",
        !string_parse_slen("1989.99", 10).present
    );

    test(
        "Numbers with trailing characters can't be parsed as signed",
        !string_parse_slen("1989 ", 10).present
    );

    test(
        "Numbers with a base lower than 2 can't be parsed an signed",
        !string_parse_slen("1989", 1).present
    );

    test(
        "Numbers with a base greater than 36 can't be parsed an signed",
        !string_parse_slen("1989", 37).present
    );
}

void test_string(void) {
    test_length();
    test_compare();
    test_find_byte();
    test_find_string();
    test_parse_ulen();
    test_parse_slen();
}
