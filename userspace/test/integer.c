#include <runtime/integer.h>

#include "test.h"

static void test_add_sign(void) {
    MaybeSlen positive = ulen_add_sign(false, 1989);

    test(
        "Adding a positive sign to an unsigned number doesn't touch it",
        positive.present && positive.value == 1989
    );

    MaybeSlen negative = ulen_add_sign(true, 1989);

    test(
        "Adding a negative sign to an unsigned number makes it negative",
        negative.present && negative.value == -1989
    );

    test(
        "Adding a positive sign to a number >= SLEN_MAX isn't possible",
        !ulen_add_sign(false, ULEN_MAX).present
    );

    test(
        "Adding a negative sign to a number >= SLEN_MAX isn't possible",
        !ulen_add_sign(true, ULEN_MAX).present
    );
}

static void test_remove_sign(void) {
    test(
        "Removing the sign from a positive number doesn't touch it",
        slen_remove_sign(1989) == 1989
    );

    test(
        "Removing the sign from a negative number makes it positive",
        slen_remove_sign(-1989) == 1989
    );

    test(
        "Removing the sign from SLEN_MAX doesn't touch it",
        slen_remove_sign(SLEN_MAX) == (ulen) SLEN_MAX
    );

    test(
        "Removing the sign from SLEN_MIN yields SLEN_MAX + 1",
        slen_remove_sign(SLEN_MIN) == (ulen) SLEN_MAX + 1
    );
}

static void test_slice_bits(void) {
    test(
        "Slicing a reasonable number of bits yields those bits",
        ulen_slice_bits(20, 2, 4) == 5
    );

    test(
        "Slicing a negative number of bits yields 0",
        ulen_slice_bits(ULEN_MAX, 10, 0) == 0
    );

    test(
        "Slicing too many bits yields the original number",
        ulen_slice_bits(ULEN_MAX, 0, 255) == ULEN_MAX
    );
}

static void test_clear_bits(void) {
    test(
        "Clearing a reasonable number of bits removes those bits",
        ulen_clear_bits(84, 2, 4) == 64
    );

    test(
        "Clearing a negative number of bits yields the original number",
        ulen_clear_bits(ULEN_MAX, 10, 0) == ULEN_MAX
    );

    test(
        "Clearing too many bits yields 0",
        ulen_clear_bits(ULEN_MAX, 0, 255) == 0
    );
}

static void test_unsigned_math(void) {
    MaybeUlen addition = ulen_add(9, 10);

    test(
        "In-range unsigned addition is defined",
        addition.present && addition.value == 19
    );

    test(
        "ULEN_MAX + (n != 0) is undefined",
        !ulen_add(ULEN_MAX, 10).present
    );

    test(
        "ULEN_MAX + ULEN_MAX is undefined",
        !ulen_add(ULEN_MAX, ULEN_MAX).present
    );

    test(
        "(n != 0) + ULEN_MAX is undefined",
        !ulen_add(9, ULEN_MAX).present
    );

    MaybeUlen subtraction = ulen_subtract(19, 9);

    test(
        "Unsigned number - smaller unsigned number is defined",
        subtraction.present && subtraction.value == 10
    );

    test(
        "Unsigned number - bigger unsigned number is undefined",
        !ulen_subtract(9, 10).present
    );

    MaybeUlen multiplication = ulen_multiply(11, 6);

    test(
        "In-range unsigned multiplication is defined",
        multiplication.present && multiplication.value == 66
    );

    test(
        "ULEN_MAX * (n > 1) is undefined",
        !ulen_multiply(ULEN_MAX, 10).present
    );

    test(
        "ULEN_MAX * ULEN_MAX is undefined",
        !ulen_multiply(ULEN_MAX, ULEN_MAX).present
    );

    test(
        "(n > 1) * ULEN_MAX is undefined",
        !ulen_multiply(9, ULEN_MAX).present
    );

    MaybeUlen division = ulen_divide(420, 69);

    test(
        "In-range unsigned division is defined",
        division.present && division.value == 6
    );

    test(
        "Unsigned number / 0 is undefined",
        !ulen_divide(1, 0).present
    );

    MaybeUlen remainder = ulen_remainder(42, 10);

    test(
        "In-range unsigned division remainder is defined",
        remainder.present && remainder.value == 2
    );

    test(
        "Unsigned number % 0 is undefined",
        !ulen_remainder(1, 0).present
    );

    MaybeUlen left_shift = ulen_shift_left(8, 1);

    test(
        "In-range unsigned left-shifting is defined",
        left_shift.present && left_shift.value == 16
    );

    test(
        "Unsigned left-shifting by too many bits is undefined",
        !ulen_shift_left(8, ULEN_MAX).present
    );

    MaybeUlen right_shift = ulen_shift_right(8, 1);

    test(
        "In-range unsigned right-shifting is defined",
        right_shift.present && right_shift.value == 4
    );

    test(
        "Unsigned right-shifting by too many bits is undefined",
        !ulen_shift_right(8, ULEN_MAX).present
    );
}

static void test_signed_math(void) {
    MaybeSlen addition = slen_add(9, -10);

    test(
        "In-range signed addition is defined",
        addition.present && addition.value == -1
    );

    test(
        "SLEN_MAX + (n > 0) is undefined",
        !slen_add(SLEN_MAX, 10).present
    );

    test(
        "SLEN_MAX + SLEN_MAX is undefined",
        !slen_add(SLEN_MAX, SLEN_MAX).present
    );

    test(
        "(n > 0) + SLEN_MAX is undefined",
        !slen_add(9, SLEN_MAX).present
    );

    test(
        "SLEN_MIN + (n < 0) is undefined",
        !slen_add(SLEN_MIN, -10).present
    );

    test(
        "SLEN_MIN + SLEN_MIN is undefined",
        !slen_add(SLEN_MIN, SLEN_MIN).present
    );

    test(
        "(n < 0) + SLEN_MIN is undefined",
        !slen_add(-9, SLEN_MIN).present
    );

    MaybeSlen subtraction = slen_subtract(9, 10);

    test(
        "In-range signed subtraction is defined",
        subtraction.present && subtraction.value == -1
    );

    test(
        "SLEN_MAX - (n < 0) is undefined",
        !slen_subtract(SLEN_MAX, -10).present
    );

    test(
        "SLEN_MIN - (n > 0) is undefined",
        !slen_subtract(SLEN_MIN, 10).present
    );

    test(
        "SLEN_MAX - SLEN_MIN is undefined",
        !slen_subtract(SLEN_MAX, SLEN_MIN).present
    );

    test(
        "SLEN_MIN - SLEN_MAX is undefined",
        !slen_subtract(SLEN_MIN, SLEN_MAX).present
    );

    MaybeSlen multiplication = slen_multiply(-3, 9);

    test(
        "In-range signed multiplication is defined",
        multiplication.present && multiplication.value == -27
    );

    test(
        "SLEN_MAX * (n > 0) is undefined",
        !slen_multiply(SLEN_MAX, 10).present
    );

    test(
        "SLEN_MIN * (n < 0) is undefined",
        !slen_multiply(SLEN_MIN, -10).present
    );

    test(
        "SLEN_MAX * SLEN_MIN is undefined",
        !slen_multiply(SLEN_MAX, SLEN_MIN).present
    );

    test(
        "SLEN_MIN * SLEN_MAX is undefined",
        !slen_multiply(SLEN_MIN, SLEN_MAX).present
    );

    test(
        "SLEN_MIN * -1 is undefined",
        !slen_multiply(SLEN_MIN, -1).present
    );

    test(
        "-1 * SLEN_MIN is undefined",
        !slen_multiply(-1, SLEN_MIN).present
    );

    MaybeSlen division = slen_divide(-9, 3);

    test(
        "In-range signed division is defined",
        division.present && division.value == -3
    );

    test(
        "Signed number / 0 is undefined",
        !slen_divide(-1, 0).present
    );

    test(
        "SLEN_MIN / -1 is undefined",
        !slen_divide(SLEN_MIN, -1).present
    );

    MaybeSlen remainder = slen_remainder(-11, 3);

    test(
        "In-range signed division remainder is defined",
        remainder.present && remainder.value == -2
    );

    test(
        "Signed number % 0 is undefined",
        !slen_remainder(-1, 0).present
    );

    test(
        "SLEN_MIN % -1 is undefined",
        !slen_remainder(SLEN_MIN, -1).present
    );

    MaybeSlen left_shift = slen_shift_left(8, 1);

    test(
        "In-range left-shifting a positive signed number is defined",
        left_shift.present && left_shift.value == 16
    );

    test(
        "Left-shifting a negative number is undefined",
        !slen_shift_left(-1, 1).present
    );

    test(
        "Signed left-shifting by too many bits is undefined",
        !slen_shift_left(8, SLEN_MAX).present
    );

    test(
        "Signed left-shifting by a negative amount is undefined",
        !slen_shift_left(8, -1).present
    );

    MaybeSlen right_shift = slen_shift_right(-8, 1);

    test(
        "In-range signed right-shifting is defined",
        right_shift.present && right_shift.value == -4
    );

    test(
        "Signed right-shifting by too many bits is undefined",
        !slen_shift_right(-8, SLEN_MAX).present
    );

    test(
        "Signed right shifting by a negative amount is undefined",
        !slen_shift_right(-8, -1).present
    );
}

void test_integer(void) {
    test_add_sign();
    test_remove_sign();
    test_slice_bits();
    test_clear_bits();

    test_unsigned_math();
    test_signed_math();
}
