#include <runtime/ascii.h>
#include <runtime/integer.h>
#include <runtime/memory.h>

bool ascii_is_non_ascii(u8 character) {
    return character >= 0x80;
}

bool ascii_is_control(u8 character) {
    if (character <= 0x1f || character == 0x7f) {
        return true;
    }

    return false;
}

bool ascii_is_whitespace(u8 character) {
    if ((character >= '\t' && character <= '\r') || character == ' ') {
        return true;
    }

    return false;
}

bool ascii_is_lowercase(u8 character) {
    if (character >= 'a' && character <= 'z') {
        return true;
    }

    return false;
}

bool ascii_is_uppercase(u8 character) {
    if (character >= 'A' && character <= 'Z') {
        return true;
    }

    return false;
}

bool ascii_is_decimal(u8 character) {
    if (character >= '0' && character <= '9') {
        return true;
    }

    return false;
}

bool ascii_is_symbol(u8 character) {
    if (
        (character >= '!' && character <= '/')
        || (character >= ':' && character <= '@')
        || (character >= '[' && character <= '`')
        || (character >= '{' && character <= '~')
    ) {
        return true;
    }

    return false;
}

void ascii_to_lowercase(u8 *array, ulen size) {
    for (ulen i = 0; i < size; i++) {
        u8 character = array[i];

        if (!ascii_is_uppercase(character)) {
            continue;
        }

        array[i] += 32;
    }
}

void ascii_to_uppercase(u8 *array, ulen size) {
    for (ulen i = 0; i < size; i++) {
        u8 character = array[i];

        if (!ascii_is_lowercase(character)) {
            continue;
        }

        array[i] -= 32;
    }
}

MaybeUlen ascii_parse_ulen(const u8 *array, ulen size, u8 base) {
    const u8 alphabet[] = "0123456789abcdefghijklmnopqrstuvwxyz";

    if (size == 0 || base < 2 || base > 36) {
        return (MaybeUlen) Nothing;
    }

    ulen result = 0;

    for (ulen i = 0; i < size; i++) {
        u8 character = array[i];
        ascii_to_lowercase(&character, 1);

        MaybeUlen digit = memory_find_byte(
            alphabet,
            sizeof(alphabet),
            character
        );

        if (!digit.present) {
            return (MaybeUlen) Nothing;
        }

        if (digit.value >= base) {
            return (MaybeUlen) Nothing;
        }

        MaybeUlen next_place = ulen_multiply(result, base);

        if (!next_place.present) {
            return (MaybeUlen) Nothing;
        }

        MaybeUlen new_result = ulen_add(next_place.value, digit.value);

        if (!new_result.present) {
            return (MaybeUlen) Nothing;
        }

        result = new_result.value;
    }

    return (MaybeUlen) Just(result);
}

MaybeSlen ascii_parse_slen(const u8 *array, ulen size, u8 base) {
    if (size == 0) {
        return (MaybeSlen) Nothing;
    }

    bool negative = false;

    if (array[0] == '-') {
        negative = true;

        array++;
        size--;
    }

    MaybeUlen unsigned_result = ascii_parse_ulen(array, size, base);

    if (!unsigned_result.present) {
        return (MaybeSlen) Nothing;
    }

    MaybeSlen result = ulen_add_sign(negative, unsigned_result.value);

    if (!result.present) {
        return (MaybeSlen) Nothing;
    }

    return (MaybeSlen) Just(result.value);
}
