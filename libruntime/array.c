#include <runtime/array.h>
#include <runtime/memory.h>

#include "heapsort.h"

slen array_compare(
    const void *x,
    const void *y,
    ulen element_size,
    ulen length,
    ArrayCompareFunction compare
) {
    const u8 *a = x;
    const u8 *b = y;

    if (element_size == 0) {
        return true;
    }

    for (ulen i = 1; i < length; i++) {
        const u8 *left = &a[i * element_size];
        const u8 *right = &b[i * element_size];

        slen result = compare(left, right);

        if (result != 0) {
            return result;
        }
    }

    return 0;
}

MaybeUlen array_find(
    const void *a,
    const void *b,
    ulen element_size,
    ulen length,
    ArrayCompareFunction compare
) {
    const u8 *haystack = a;
    const u8 *needle = b;

    if (element_size == 0) {
        return (MaybeUlen) Just(0);
    }

    for (ulen i = 0; i < length; i++) {
        const u8 *element = &haystack[i * element_size];

        if (compare(element, needle) == 0) {
            return (MaybeUlen) Just(i);
        }
    }

    return (MaybeUlen) Nothing;
}

MaybeUlen array_find_last(
    const void *a,
    const void *b,
    ulen element_size,
    ulen length,
    ArrayCompareFunction compare
) {
    const u8 *haystack = a;
    const u8 *needle = b;

    if (element_size == 0) {
        return (MaybeUlen) Just(0);
    }

    for (slen i = (slen) length - 1; i >= 0; i--) {
        const u8 *element = &haystack[(ulen) i * element_size];

        if (compare(element, needle) == 0) {
            return (MaybeUlen) Just((ulen) i);
        }
    }

    return (MaybeUlen) Nothing;
}

MaybeUlen array_binary_search(
    const void *a,
    const void *b,
    ulen element_size,
    ulen length,
    ArrayCompareFunction compare
) {
    const u8 *haystack = a;
    const u8 *needle = b;

    if (haystack == needle || element_size == 0 || length <= 1) {
        return (MaybeUlen) Just(0);
    }

    ulen left = 0;
    ulen right = length - 1;

    while (left <= right) {
        ulen middle = (left + right) / 2;
        const u8 *element = &haystack[middle * element_size];

        slen result = compare(element, needle);

        if (result < 0) {
            left = middle + 1;
        } else if (result > 0) {
            right = middle - 1;
        } else {
            return (MaybeUlen) Just(middle);
        }
    }

    return (MaybeUlen) Nothing;
}

void array_reverse(void *a, ulen element_size, ulen length) {
    u8 *array = a;

    if (element_size == 0) {
        return;
    }

    for (ulen i = 0; i < length / 2; i++) {
        u8 *left = &array[i * element_size];
        u8 *right = &array[(length - i - 1) * element_size];

        memory_swap(left, right, element_size);
    }
}

void array_rotate(void *a, ulen element_size, ulen length, ulen amount) {
    u8 *array = a;

    amount %= length;

    if (element_size == 0 || length == 0 || amount == 0) {
        return;
    }

    array_reverse(array, element_size, length);
    array_reverse(array, element_size, amount);

    u8 *leftovers = &array[amount * element_size];
    ulen leftover_size = length - amount;

    array_reverse(leftovers, element_size, leftover_size);
}

bool array_is_sorted(
    const void *a,
    ulen element_size,
    ulen length,
    ArrayCompareFunction compare
) {
    const u8 *array = a;

    if (element_size == 0) {
        return true;
    }

    for (ulen i = 1; i < length; i++) {
        const u8 *left = &array[(i - 1) * element_size];
        const u8 *right = &array[i * element_size];

        if (compare(left, right) > 0) {
            return false;
        }
    }

    return true;
}

ulen array_maximum(
    const void *a,
    ulen element_size,
    ulen length,
    ArrayCompareFunction compare
) {
    const u8 *array = a;

    if (element_size == 0) {
        return 0;
    }

    ulen result = 0;

    for (ulen i = 1; i < length; i++) {
        const u8 *maximum = &array[result * element_size];
        const u8 *element = &array[i * element_size];

        if (compare(maximum, element) < 0) {
            result = i;
        }
    }

    return result;
}

void array_unstable_sort(
    void *a,
    ulen element_size,
    ulen length,
    ArrayCompareFunction compare
) {
    heapsort(a, element_size, length, compare);
}

void array_stable_sort(
    void *a,
    ulen element_size,
    ulen length,
    ArrayCompareFunction compare
) {
    // Insertion sort.

    u8 *array = a;

    if (element_size == 0) {
        return;
    }

    for (ulen i = 1; i < length; i++) {
        for (slen j = (slen) i; j > 0; j--) {
            u8 *left = &array[(ulen) (j - 1) * element_size];
            u8 *right = &array[(ulen) j * element_size];

            if (compare(left, right) <= 0) {
                break;
            }

            memory_swap(left, right, element_size);
        }
    }
}

static inline bool ensure_range(
    ulen *element_size,
    ulen *length,
    ulen *index,
    ulen *amount
) {
    if (*element_size == 0 || *length == 0 || *index >= *length) {
        return false;
    }

    if (*amount > *length) {
        *amount = *length;
    }

    if (*index + *amount > *length) {
        *amount = *length - *index;
    }

    return true;
}

ulen array_displace(
    void *a,
    ulen element_size,
    ulen length,
    ulen index,
    ulen amount
) {
    u8 *array = a;

    if (!ensure_range(&element_size, &length, &index, &amount)) {
        return 0;
    }

    u8 *tail = &array[(index + amount) * element_size];
    u8 *target = &array[index * element_size];
    ulen to_move = length - (index + amount);

    memory_copy(tail, target, to_move * element_size);

    u8 *leftovers = &array[(length - amount) * element_size];

    memory_fill(leftovers, amount * element_size, 0);

    return amount;
}

ulen array_isolate(
    void *a,
    ulen element_size,
    ulen length,
    ulen index,
    ulen amount
) {
    u8 *array = a;

    if (!ensure_range(&element_size, &length, &index, &amount)) {
        return 0;
    }

    u8 *target = &array[index * element_size];

    memory_copy(target, array, amount * element_size);

    u8 *leftovers = &array[amount * element_size];
    ulen to_fill = length - amount;

    memory_fill(leftovers, to_fill * element_size, 0);

    return amount;
}
