#include <runtime/ascii.h>
#include <runtime/memory.h>
#include <runtime/string.h>

ulen string_length(str string) {
    ulen i = 0;

    while (string[i] != '\0') {
        i++;
    }

    return i;
}

slen string_compare(str a, str b) {
    ulen a_length = string_length(a);
    ulen b_length = string_length(b);

    if (a_length > b_length) {
        return 1;
    } else if (a_length < b_length) {
        return -1;
    }

    return memory_compare(a, b, a_length);
}

MaybeUlen string_find_byte(str string, u8 byte) {
    ulen length = string_length(string);

    return memory_find_byte(string, length, byte);
}

MaybeUlen string_find_string(str haystack, str needle) {
    ulen a_length = string_length(haystack);
    ulen b_length = string_length(needle);

    return memory_find_memory(haystack, a_length, needle, b_length);
}

MaybeUlen string_parse_ulen(str string, u8 base) {
    ulen length = string_length(string);

    return ascii_parse_ulen((const u8 *) string, length, base);
}

MaybeSlen string_parse_slen(str string, u8 base) {
    ulen length = string_length(string);

    return ascii_parse_slen((const u8 *) string, length, base);
}
