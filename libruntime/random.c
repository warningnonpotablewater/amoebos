#include <runtime/memory.h>
#include <runtime/random.h>

struct random_state random_new(u32 seed) {
    // `random_next` expects a non-zero seed.

    if (seed == 0) {
        seed = 1;
    }

    return (struct random_state) {.last = seed};
}

u32 random_next(struct random_state *state) {
    // xorshift32 RNG algorithm.

    state->last ^= state->last << 13;
    state->last ^= state->last >> 17;
    state->last ^= state->last << 5;

    return state->last;
}

bool random_bool(struct random_state *state) {
    return random_next(state) & 1;
}

// The [`floor`; `ceiling`] range is closed.

u32 random_u32(struct random_state *state, u32 floor, u32 ceiling) {
    if (floor == ceiling) {
        return floor;
    }

    if (floor > ceiling) {
        memory_swap(&floor, &ceiling, sizeof(u32));
    }

    u32 result = random_next(state);

    if (floor == U32_MIN && ceiling == U32_MAX) {
        return result;
    }

    // This likely provides a really ununiform distribution. Don't use it for
    // anything serious.

    u32 range = ceiling - floor + 1;

    result %= range;
    result += floor;

    return result;
}

s32 random_s32(struct random_state *state, s32 floor, s32 ceiling) {
    if (floor == ceiling) {
        return floor;
    }

    if (floor > ceiling) {
        memory_swap(&floor, &ceiling, sizeof(s32));
    }

    if (floor == S32_MIN && ceiling == S32_MAX) {
        return (s32) random_next(state);
    }

    // Relies on unsigned integer overflow wrapping.

    u32 range = (u32) ceiling - (u32) floor;

    return (s32) (random_u32(state, 0, range) + (ulen) floor);
}

void random_shuffle(
    struct random_state *state,
    void *a,
    ulen element_size,
    ulen length
) {
    u8 *source = a;

    for (slen i = (slen) length - 1; i > 0; i--) {
        u32 j = random_u32(state, 0, (u32) i);

        u8 *left = &source[(ulen) i * element_size];
        u8 *right = &source[j * element_size];

        memory_swap(left, right, element_size);
    }
}
