#include <runtime/integer.h>

MaybeSlen ulen_add_sign(bool negative, ulen number) {
    if (negative) {
        if (number > (ulen) SLEN_MAX + 1) {
            return (MaybeSlen) Nothing;
        }

        return (MaybeSlen) Just(-(slen) number);
    } else {
        if (number > SLEN_MAX) {
            return (MaybeSlen) Nothing;
        }

        return (MaybeSlen) Just((slen) number);
    }
}

ulen slen_remove_sign(slen number) {
    if (number == SLEN_MIN) {
        return (ulen) SLEN_MAX + 1;
    }

    if (number < 0) {
        return (ulen) -number;
    }

    return (ulen) number;
}

// Unsigned integer overflow is not undefined behavior, so it's enough to check
// for it after performing the operation.

MaybeUlen ulen_add(ulen a, ulen b) {
    ulen result = a + b;

    if (b != 0 && result < a) {
        return (MaybeUlen) Nothing;
    }

    return (MaybeUlen) Just(result);
}

MaybeUlen ulen_subtract(ulen a, ulen b) {
    ulen result = a - b;

    if (b != 0 && result > a) {
        return (MaybeUlen) Nothing;
    }

    return (MaybeUlen) Just(result);
}

MaybeUlen ulen_multiply(ulen a, ulen b) {
    ulen result = a * b;

    if (b != 0 && result / b != a) {
        return (MaybeUlen) Nothing;
    }

    return (MaybeUlen) Just(result);
}

MaybeUlen ulen_divide(ulen a, ulen b) {
    if (b == 0) {
        return (MaybeUlen) Nothing;
    }

    return (MaybeUlen) Just(a / b);
}

MaybeUlen ulen_remainder(ulen a, ulen b) {
    if (b == 0) {
        return (MaybeUlen) Nothing;
    }

    return (MaybeUlen) Just(a % b);
}

// When shifting unsigned integers, undefined behavior can only occur if the
// shift amount is too big.

MaybeUlen ulen_shift_left(ulen number, ulen amount) {
    if (amount >= XLEN_BITS) {
        return (MaybeUlen) Nothing;
    }

    return (MaybeUlen) Just(number << amount);
}

MaybeUlen ulen_shift_right(ulen number, ulen amount) {
    if (amount >= XLEN_BITS) {
        return (MaybeUlen) Nothing;
    }

    return (MaybeUlen) Just(number >> amount);
}

// Signed integer overflow, on other hand, is undefined behavior, so the
// operation needs to be performed only after the check.

MaybeSlen slen_add(slen a, slen b) {
    bool overflows = b > 0 && a > SLEN_MAX - b;
    bool underflows = b < 0 && a < SLEN_MIN - b;

    if (overflows || underflows) {
        return (MaybeSlen) Nothing;
    }

    return (MaybeSlen) Just(a + b);
}

MaybeSlen slen_subtract(slen a, slen b) {
    bool overflows = b < 0 && a > SLEN_MAX + b;
    bool underflows = b > 0 && a < SLEN_MIN + b;

    if (overflows || underflows) {
        return (MaybeSlen) Nothing;
    }

    return (MaybeSlen) Just(a - b);
}

MaybeSlen slen_multiply(slen a, slen b) {
    bool overflows = false;
    bool underflows = false;

    if (a < 0) {
        if (b < 0) {
            overflows = a < SLEN_MAX / b;
        } else if (b > 0) {
            underflows = a < SLEN_MIN / b;
        }
    } else {
        if (b < 0) {
            underflows = a > SLEN_MIN / b;
        } else if (b > 0) {
            overflows = a > SLEN_MAX / b;
        }
    }

    if (overflows || underflows) {
        return (MaybeSlen) Nothing;
    }

    return (MaybeSlen) Just(a * b);
}

MaybeSlen slen_divide(slen a, slen b) {
    bool by_zero = b == 0;
    bool overflows = a == SLEN_MIN && b == -1;

    if (by_zero || overflows) {
        return (MaybeSlen) Nothing;
    }

    return (MaybeSlen) Just(a / b);
}

MaybeSlen slen_remainder(slen a, slen b) {
    bool by_zero = b == 0;
    bool overflows = a == SLEN_MIN && b == -1;

    if (by_zero || overflows) {
        return (MaybeSlen) Nothing;
    }

    return (MaybeSlen) Just(a % b);
}

// When shifting signed integers, undefined behavior can also occur depending on
// the sign of the operands.

MaybeSlen slen_shift_left(slen number, slen amount) {
    bool negative_number = number < 0;
    bool invalid_amount = amount < 0 || amount >= (slen) XLEN_BITS;
    bool overflows = !invalid_amount && number > (SLEN_MAX >> amount);

    if (negative_number || invalid_amount || overflows) {
        return (MaybeSlen) Nothing;
    }

    return (MaybeSlen) Just(number << amount);
}

MaybeSlen slen_shift_right(slen number, slen amount) {
    // If number is negative, an arithmetic shift is assumed.

    if (amount < 0 || amount >= (slen) XLEN_BITS) {
        return (MaybeSlen) Nothing;
    }

    return (MaybeSlen) Just(number >> amount);
}

// The [start; end] range is closed.

ulen ulen_slice_bits(ulen value, u8 start, u8 end) {
    slen length = (slen) end - (slen) start + 1;

    if (length <= 0) {
        return 0;
    }
    if (length >= (slen) XLEN_BITS) {
        return value;
    }

    ulen mask = ((ulen) 1 << length) - 1;
    ulen overlap = (value >> start) & mask;

    return overlap;
}

ulen ulen_clear_bits(ulen value, u8 start, u8 end) {
    slen length = (slen) end - (slen) start + 1;

    if (length <= 0) {
        return value;
    }
    if (length >= (slen) XLEN_BITS) {
        return 0;
    }

    ulen mask = (((ulen) 1 << length) - 1) << start;
    ulen overlap = value & ~mask;

    return overlap;
}
