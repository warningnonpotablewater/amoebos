#include <runtime/format.h>
#include <runtime/print.h>

ulen print(u8 *data, ulen size, str recipe, ...) {
    va_list anchor;

    va_start(anchor, recipe);

    struct format_buffer buffer = format_buffer_new(data, size);

    for (ulen i = 0; recipe[i] != '\0'; i++) {
        u8 character = (u8) recipe[i];

        if (character != '%') {
            format_character(&buffer, character);

            continue;
        }

        character = (u8) recipe[++i];

        void *pointer;
        str string;

        // Integer conversions assume that the `l` flag is always on.

        ulen integer;
        slen signed_integer;

        switch (character) {
        case '%':
            format_character(&buffer, '%');

            break;
        case 'p':
            pointer = va_arg(anchor, void *);

            format_ulen(&buffer, (ulen) pointer, 16, 8);

            break;
        case 's':
            string = va_arg(anchor, str);

            format_string(&buffer, string);

            break;
        case 'c':
            integer = va_arg(anchor, ulen); // That's what the standard says.

            format_character(&buffer, (u8) integer);

            break;
        case 'x':
            integer = va_arg(anchor, ulen);

            format_ulen(&buffer, integer, 16, 0);

            break;
        case 'u':
            integer = va_arg(anchor, ulen);

            format_ulen(&buffer, integer, 10, 0);

            break;
        case 'o':
            integer = va_arg(anchor, ulen);

            format_ulen(&buffer, integer, 8, 0);

            break;
        case 'd': // FALLTHROUGH.
        case 'i':
            signed_integer = va_arg(anchor, slen);

            format_slen(&buffer, signed_integer, 10, 0);

            break;
        default:
            // "The behaviour is undefined."

            crash();
        }
    }

    va_end(anchor);

    return buffer.used;
}
