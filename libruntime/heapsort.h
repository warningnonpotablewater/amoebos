#ifndef HEAPSORT_H
#define HEAPSORT_H

#include <runtime/memory.h>
#include <runtime/primitives.h>

static inline ulen parent_of(ulen child) {
    return (child - 1) / 2;
}

static inline ulen left_child_of(ulen parent) {
    return 2 * parent + 1;
}

static inline ulen right_child_of(ulen parent) {
    return 2 * parent + 2;
}

static void sift_down(
    u8 *array,
    ulen element_size,
    ulen length,
    ArrayCompareFunction compare,
    ulen root
) {
    ulen end = length - 1;

    for (;;) {
        ulen left_child = left_child_of(root);
        bool left_child_exists = left_child <= end;

        if (!left_child_exists) {
            break;
        }

        ulen bigger_child = left_child;

        ulen right_child = right_child_of(root);
        bool right_child_exists = right_child <= end;

        if (right_child_exists) {
            u8 *left = &array[left_child * element_size];
            u8 *right = &array[right_child * element_size];

            if (compare(left, right) < 0) {
                bigger_child = right_child;
            }
        }

        u8 *top = &array[root * element_size];
        u8 *bottom = &array[bigger_child * element_size];

        if (compare(top, bottom) >= 0) {
            break;
        }

        memory_swap(top, bottom, element_size);

        root = bigger_child;
    }
}

static void heapify(
    u8 *array,
    ulen element_size,
    ulen length,
    ArrayCompareFunction compare
) {
    ulen last_parent = parent_of(length - 1);

    for (slen root = (slen) last_parent; root >= 0; root--) {
        sift_down(array, element_size, length, compare, (ulen) root);
    }
}

static void heapsort(
    u8 *array,
    ulen element_size,
    ulen length,
    ArrayCompareFunction compare
) {
    if (element_size == 0 || length <= 1) {
        return;
    }

    heapify(array, element_size, length, compare);

    for (slen end = (slen) length - 1; end > 0; end--) {
        u8 *first = array;
        u8 *last = &array[(ulen) end * element_size];

        memory_swap(first, last, element_size);

        sift_down(array, element_size, (ulen) end, compare, 0);
    }
}

#endif
