#include <runtime/memory.h>

// GCC needs these functions to be present even if -fno-builtin is passed. See
// https://gcc.gnu.org/onlinedocs/gcc/Link-Options.html

int memcmp(const void *a, const void *b, ulen size) {
    return (int) memory_compare(a, b, size);
}

void *memset(void *a, int value, ulen size) {
    memory_fill(a, size, (u8) value);

    return a;
}

void *memmove(void *b, const void *a, ulen size) {
    memory_copy(a, b, size);

    return b;
}

void *memcpy(void *b, const void *a, ulen size) {
    return memmove(b, a, size);
}
