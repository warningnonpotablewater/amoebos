#include <runtime/memory.h>

#include "generic.h"

// The following functions are optimized to work with XLEN-sized chunks whenever
// possible. Rationale: the more data you access at once, the faster you go.
// This probably indirectly violates the strict aliasing rule, but let's be
// real, trying to comply with it here would be utter insanity.

void memory_copy(const void *a, void *b, ulen size) {
    if (ALIGNED3(ulen, a, b, size)) {
        size /= sizeof(ulen);

        const ulen *source = a;
        ulen *destination = b;

        COPY(source, destination, size);
    } else {
        const u8 *source = a;
        u8 *destination = b;

        COPY(source, destination, size);
    }
}

slen memory_compare(const void *a, const void *b, ulen size) {
    // When dealing with multiple bytes at a time, comparisons need to be
    // big-endian unless we're just trying to find out whether 2 regions are
    // different. As such, if they are different, we need to check them again
    // byte by byte to find the difference, which is only worth doing if the
    // region size is big enough.

    bool worth_optimizing = size >= sizeof(ulen) * 2;

    if (ALIGNED3(ulen, a, b, size) && worth_optimizing) {
        size /= sizeof(ulen);

        const ulen *left = a;
        const ulen *right = b;

        for (ulen i = 0; i < size; i++) {
            if (left[i] != right[i]) {
                const u8 *left_bytes = (void *) &left[i];
                const u8 *right_bytes = (void *) &right[i];

                COMPARE(left_bytes, right_bytes, sizeof(ulen));
            }
        }
    } else {
        const u8 *left = a;
        const u8 *right = b;

        COMPARE(left, right, size);
    }

    return 0;
}

void memory_fill(void *a, ulen size, u8 value) {
    if (ALIGNED2(ulen, a, size)) {
        size /= sizeof(ulen);

        ulen *destination = a;
        ulen wide_value = 0;

        for (ulen bits = 0; bits < XLEN_BITS; bits += 8) {
            wide_value |= value << bits;
        }

        FILL(destination, size, wide_value);
    } else {
        u8 *destination = a;

        FILL(destination, size, value);
    }
}

void memory_swap(void *a, void *b, ulen size) {
    if (ALIGNED3(ulen, a, b, size)) {
        size /= sizeof(ulen);

        ulen *left = a;
        ulen *right = b;

        SWAP(ulen, left, right, size);
    } else {
        u8 *left = a;
        u8 *right = b;

        SWAP(u8, left, right, size);
    }
}

MaybeUlen memory_find_byte(const void *a, ulen size, u8 byte) {
    const u8 *source = a;

    for (ulen i = 0; i < size; i++) {
        if (source[i] == byte) {
            return (MaybeUlen) Just(i);
        }
    }

    return (MaybeUlen) Nothing;
}

MaybeUlen memory_find_memory(
    const void *a,
    ulen a_length,
    const void *b,
    ulen b_length
) {
    const u8 *haystack = a;
    const u8 *needle = b;

    if (b_length > a_length) {
        return (MaybeUlen) Nothing;
    }

    if (haystack == needle || b_length == 0) {
        return (MaybeUlen) Just(0);
    }

    for (ulen i = 0; i <= a_length - b_length; i++) {
        if (memory_compare(&haystack[i], needle, b_length) == 0) {
            return (MaybeUlen) Just(i);
        }
    }

    return (MaybeUlen) Nothing;
}
