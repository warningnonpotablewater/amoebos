#ifndef GENERIC_H
#define GENERIC_H

#include <runtime/primitives.h>

#define ALIGNED(type, variable) ((ulen) (variable) % alignof(type) == 0)

#define ALIGNED2(type, a, b) (ALIGNED(type, a) && ALIGNED(type, b))
#define ALIGNED3(type, a, b, c)\
    (ALIGNED(type, a) && ALIGNED(type, b) && ALIGNED(type, c))

#define COPY(source, destination, size)\
    if (source > destination) {\
        for (ulen i = 0; i < size; i++) {\
            destination[i] = source[i];\
        }\
    } else if (source < destination) {\
        for (slen i = (slen) size - 1; i >= 0; i--) {\
            destination[i] = source[i];\
        }\
    }

#define COMPARE(left, right, size)\
    for (ulen i = 0; i < size; i++) {\
        if (left[i] < right[i]) {\
            return -1;\
        } else if (left[i] > right[i]) {\
            return 1;\
        }\
    }

#define FILL(destination, size, value)\
    for (ulen i = 0; i < size; i++) {\
        destination[i] = value;\
    }

#define SWAP(type, left, right, size)\
    for (ulen i = 0; i < size; i++) {\
        type old_left = left[i];\
        \
        left[i] = right[i];\
        right[i] = old_left;\
    }

#endif
