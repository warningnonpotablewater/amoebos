#include <runtime/array.h>
#include <runtime/ascii.h>
#include <runtime/format.h>
#include <runtime/integer.h>
#include <runtime/memory.h>
#include <runtime/string.h>

// Enough to fit the largest printable unsigned integer using the smallest
// allowed base (2).

#define MAX_NUMBER_SIZE XLEN_BITS

struct format_buffer format_buffer_new(u8 *source, ulen capacity) {
    ulen already_occupied = 0;

    if (capacity != 0) {
        already_occupied = string_length((str) source);
        capacity -= already_occupied + 1; // To make space for the null byte.
    }

    u8 *data = source + already_occupied;

    return (struct format_buffer) {data, capacity, 0};
}

void format_continue(struct format_buffer *buffer) {
    ulen already_occupied = buffer->used;
    u8 *data = &buffer->data[already_occupied];

    buffer->used = 0;
    buffer->capacity -= already_occupied;
    buffer->data = data;
}

void format_clear(struct format_buffer *buffer) {
    buffer->used = 0;

    memory_fill(buffer->data, buffer->capacity, 0);
}

static inline bool ensure_that_fits(struct format_buffer *buffer, ulen *size) {
    ulen space_left = buffer->capacity - buffer->used;

    if (*size > space_left) {
        *size = space_left;

        return false;
    }

    return true;
}

static inline void null_terminate(struct format_buffer *buffer) {
    // Needs to be checked to not accidentally write the null terminator to an
    // invalid location.

    if (buffer->capacity == 0) {
        return;
    }

    buffer->data[buffer->used] = 0;
}

bool format_character(struct format_buffer *buffer, u8 character) {
    if (buffer->used >= buffer->capacity) {
        return false;
    }

    buffer->data[buffer->used] = character;

    buffer->used++;
    null_terminate(buffer);

    return true;
}

bool format_memory(
    struct format_buffer *buffer,
    const void *source,
    ulen size
) {
    bool fits = ensure_that_fits(buffer, &size);

    memory_copy(source, &buffer->data[buffer->used], size);

    buffer->used += size;
    null_terminate(buffer);

    return fits;
}

bool format_string(struct format_buffer *buffer, str string) {
    return format_memory(buffer, string, string_length(string));
}

bool format_ulen(
    struct format_buffer *buffer,
    ulen number,
    u8 base,
    u8 padding
) {
    const u8 alphabet[] = "0123456789abcdefghijklmnopqrstuvwxyz";

    u8 digits[MAX_NUMBER_SIZE + 1] = {0};
    ulen last_digit = 0;

    if (padding > MAX_NUMBER_SIZE) {
        padding = MAX_NUMBER_SIZE;
    }

    if (base < 2 || base > 36) {
        goto pad;
    }

    if (number == 0) {
        digits[last_digit] = '0';
        last_digit++;
    }

    while (number > 0) {
        digits[last_digit] = alphabet[number % base];

        number /= base;
        last_digit++;
    }

pad:
    for (; last_digit < padding; last_digit++) {
        digits[last_digit] = '0';
    }

    array_reverse(digits, sizeof(u8), last_digit);

    return format_string(buffer, (str) digits);
}

bool format_slen(
    struct format_buffer *buffer,
    slen number,
    u8 base,
    u8 padding
) {
    if (number < 0) {
        format_character(buffer, '-');
    }

    ulen unsigned_number = slen_remove_sign(number);

    return format_ulen(buffer, unsigned_number, base, padding);
}

bool format_pad_start(struct format_buffer *buffer, ulen amount, u8 character) {
    if (amount <= buffer->used) {
        return true;
    }

    amount -= buffer->used;
    bool fits = ensure_that_fits(buffer, &amount);

    memory_copy(buffer->data, &buffer->data[amount], buffer->used);
    memory_fill(buffer->data, amount, character);

    buffer->used += amount;
    null_terminate(buffer);

    return fits;
}

bool format_pad_end(struct format_buffer *buffer, ulen amount, u8 character) {
    if (amount <= buffer->used) {
        return true;
    }

    amount -= buffer->used;
    bool fits = ensure_that_fits(buffer, &amount);

    memory_fill(&buffer->data[buffer->used], amount, character);

    buffer->used += amount;
    null_terminate(buffer);

    return fits;
}

void format_displace(struct format_buffer *buffer, ulen index, ulen amount) {
    ulen displaced = array_displace(
        buffer->data,
        sizeof(u8),
        buffer->used,
        index,
        amount
    );

    buffer->used -= displaced;
}

void format_isolate(struct format_buffer *buffer, ulen index, ulen amount) {
    ulen isolated = array_isolate(
        buffer->data,
        sizeof(u8),
        buffer->used,
        index,
        amount
    );

    buffer->used = isolated;
}

void format_lowercase(struct format_buffer *buffer) {
    ascii_to_lowercase(buffer->data, buffer->used);
}

void format_uppercase(struct format_buffer *buffer) {
    ascii_to_uppercase(buffer->data, buffer->used);
}
