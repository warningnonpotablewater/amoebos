#!/bin/sh

set -uex

KERNEL="$1"
USERSPACE="$2"
FILESYSTEM="$3"
OUTPUT="$4"

cp "${KERNEL}" "${OUTPUT}"
dd if="${USERSPACE}" seek=2M bs=1 of="${OUTPUT}"
dd if="${FILESYSTEM}" seek=4M bs=1 of="${OUTPUT}"
